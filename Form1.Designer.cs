﻿namespace Project_Copy
{
    partial class FrmCopyProjectProposal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.CBOProjCompany = new System.Windows.Forms.ComboBox();
            this.CBOFrameworkContract = new System.Windows.Forms.ComboBox();
            this.RBProject = new System.Windows.Forms.RadioButton();
            this.RBProposal = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.CBOProjectProposal = new System.Windows.Forms.ComboBox();
            this.RBCopyFrameWorkContract = new System.Windows.Forms.RadioButton();
            this.RBCopyProject = new System.Windows.Forms.RadioButton();
            this.BTNRoles = new System.Windows.Forms.Button();
            this.BTNExit = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.TXTNewProjectCode = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.TXTNewFrameWorkContract = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.TXTNoJNBProjects = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.TXTNoMMProjects = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.TXTNoOfJBAProjects = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label5 = new System.Windows.Forms.Label();
            this.CBOFCCompany = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.TXTFCTitle = new System.Windows.Forms.TextBox();
            this.TTCodeFormat = new System.Windows.Forms.ToolTip(this.components);
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(21, 125);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(267, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Select the Company you wish to copy the Project from?";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(22, 171);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(262, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Which Framework Contract do you wish to copy from?";
            // 
            // CBOProjCompany
            // 
            this.CBOProjCompany.FormattingEnabled = true;
            this.CBOProjCompany.Location = new System.Drawing.Point(365, 122);
            this.CBOProjCompany.Margin = new System.Windows.Forms.Padding(2);
            this.CBOProjCompany.Name = "CBOProjCompany";
            this.CBOProjCompany.Size = new System.Drawing.Size(210, 21);
            this.CBOProjCompany.TabIndex = 1;
            this.CBOProjCompany.SelectedIndexChanged += new System.EventHandler(this.CBOProjCompany_SelectedIndexChanged);
            // 
            // CBOFrameworkContract
            // 
            this.CBOFrameworkContract.FormattingEnabled = true;
            this.CBOFrameworkContract.Location = new System.Drawing.Point(365, 168);
            this.CBOFrameworkContract.Margin = new System.Windows.Forms.Padding(2);
            this.CBOFrameworkContract.Name = "CBOFrameworkContract";
            this.CBOFrameworkContract.Size = new System.Drawing.Size(210, 21);
            this.CBOFrameworkContract.TabIndex = 4;
            this.CBOFrameworkContract.SelectedIndexChanged += new System.EventHandler(this.CBOFrameworkContract_SelectedIndexChanged);
            // 
            // RBProject
            // 
            this.RBProject.AutoSize = true;
            this.RBProject.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RBProject.Location = new System.Drawing.Point(4, 17);
            this.RBProject.Margin = new System.Windows.Forms.Padding(2);
            this.RBProject.Name = "RBProject";
            this.RBProject.Size = new System.Drawing.Size(65, 17);
            this.RBProject.TabIndex = 9;
            this.RBProject.TabStop = true;
            this.RBProject.Text = "Project";
            this.RBProject.UseVisualStyleBackColor = true;
            this.RBProject.CheckedChanged += new System.EventHandler(this.RBProject_CheckedChanged);
            // 
            // RBProposal
            // 
            this.RBProposal.AutoSize = true;
            this.RBProposal.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RBProposal.Location = new System.Drawing.Point(76, 17);
            this.RBProposal.Margin = new System.Windows.Forms.Padding(2);
            this.RBProposal.Name = "RBProposal";
            this.RBProposal.Size = new System.Drawing.Size(74, 17);
            this.RBProposal.TabIndex = 10;
            this.RBProposal.TabStop = true;
            this.RBProposal.Text = "Proposal";
            this.RBProposal.UseVisualStyleBackColor = true;
            this.RBProposal.CheckedChanged += new System.EventHandler(this.RBProposal_CheckedChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(22, 311);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(231, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Which project or proposal do you wish to copy?";
            // 
            // CBOProjectProposal
            // 
            this.CBOProjectProposal.FormattingEnabled = true;
            this.CBOProjectProposal.Location = new System.Drawing.Point(365, 303);
            this.CBOProjectProposal.Margin = new System.Windows.Forms.Padding(2);
            this.CBOProjectProposal.Name = "CBOProjectProposal";
            this.CBOProjectProposal.Size = new System.Drawing.Size(210, 21);
            this.CBOProjectProposal.TabIndex = 11;
            this.CBOProjectProposal.SelectedIndexChanged += new System.EventHandler(this.CBOProjectProposal_SelectedIndexChanged);
            // 
            // RBCopyFrameWorkContract
            // 
            this.RBCopyFrameWorkContract.AutoSize = true;
            this.RBCopyFrameWorkContract.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RBCopyFrameWorkContract.Location = new System.Drawing.Point(4, 17);
            this.RBCopyFrameWorkContract.Margin = new System.Windows.Forms.Padding(2);
            this.RBCopyFrameWorkContract.Name = "RBCopyFrameWorkContract";
            this.RBCopyFrameWorkContract.Size = new System.Drawing.Size(138, 17);
            this.RBCopyFrameWorkContract.TabIndex = 2;
            this.RBCopyFrameWorkContract.TabStop = true;
            this.RBCopyFrameWorkContract.Text = "Framework Contract";
            this.RBCopyFrameWorkContract.UseVisualStyleBackColor = true;
            this.RBCopyFrameWorkContract.CheckedChanged += new System.EventHandler(this.RBCopyFrameWorkContract_CheckedChanged);
            // 
            // RBCopyProject
            // 
            this.RBCopyProject.AutoSize = true;
            this.RBCopyProject.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RBCopyProject.Location = new System.Drawing.Point(157, 17);
            this.RBCopyProject.Margin = new System.Windows.Forms.Padding(2);
            this.RBCopyProject.Name = "RBCopyProject";
            this.RBCopyProject.Size = new System.Drawing.Size(65, 17);
            this.RBCopyProject.TabIndex = 3;
            this.RBCopyProject.TabStop = true;
            this.RBCopyProject.Text = "Project";
            this.RBCopyProject.UseVisualStyleBackColor = true;
            this.RBCopyProject.CheckedChanged += new System.EventHandler(this.RBCopyProject_CheckedChanged);
            // 
            // BTNRoles
            // 
            this.BTNRoles.Location = new System.Drawing.Point(365, 366);
            this.BTNRoles.Margin = new System.Windows.Forms.Padding(2);
            this.BTNRoles.Name = "BTNRoles";
            this.BTNRoles.Size = new System.Drawing.Size(105, 27);
            this.BTNRoles.TabIndex = 16;
            this.BTNRoles.Text = "Enter Project Data";
            this.BTNRoles.UseVisualStyleBackColor = true;
            this.BTNRoles.Click += new System.EventHandler(this.BTNRoles_Click);
            // 
            // BTNExit
            // 
            this.BTNExit.Location = new System.Drawing.Point(365, 406);
            this.BTNExit.Margin = new System.Windows.Forms.Padding(2);
            this.BTNExit.Name = "BTNExit";
            this.BTNExit.Size = new System.Drawing.Size(59, 27);
            this.BTNExit.TabIndex = 17;
            this.BTNExit.Text = "Exit";
            this.BTNExit.UseVisualStyleBackColor = true;
            this.BTNExit.Click += new System.EventHandler(this.BTNExit_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(86, 28);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(399, 26);
            this.label8.TabIndex = 18;
            this.label8.Text = "Copy Framework Contract or Project";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.RBCopyFrameWorkContract);
            this.groupBox1.Controls.Add(this.RBCopyProject);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(28, 76);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(249, 42);
            this.groupBox1.TabIndex = 19;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Copy From";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.RBProject);
            this.groupBox2.Controls.Add(this.RBProposal);
            this.groupBox2.Location = new System.Drawing.Point(24, 267);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox2.Size = new System.Drawing.Size(150, 42);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Project Types";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(22, 333);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(125, 13);
            this.label3.TabIndex = 21;
            this.label3.Text = "New Project Code Prefix:";
            // 
            // TXTNewProjectCode
            // 
            this.TXTNewProjectCode.Location = new System.Drawing.Point(365, 330);
            this.TXTNewProjectCode.Margin = new System.Windows.Forms.Padding(2);
            this.TXTNewProjectCode.Name = "TXTNewProjectCode";
            this.TXTNewProjectCode.Size = new System.Drawing.Size(116, 20);
            this.TXTNewProjectCode.TabIndex = 12;
            this.TXTNewProjectCode.TextChanged += new System.EventHandler(this.TXTNewProjectCode_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(21, 196);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(158, 13);
            this.label6.TabIndex = 23;
            this.label6.Text = "New Framework Contract Code:";
            // 
            // TXTNewFrameWorkContract
            // 
            this.TXTNewFrameWorkContract.Location = new System.Drawing.Point(365, 193);
            this.TXTNewFrameWorkContract.Name = "TXTNewFrameWorkContract";
            this.TXTNewFrameWorkContract.Size = new System.Drawing.Size(116, 20);
            this.TXTNewFrameWorkContract.TabIndex = 7;
            this.TXTNewFrameWorkContract.TextChanged += new System.EventHandler(this.TXTNewFrameWorkContract_TextChanged);
            this.TXTNewFrameWorkContract.Leave += new System.EventHandler(this.TXTNewFrameWorkContract_LostFocus);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(9, 16);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(168, 13);
            this.label9.TabIndex = 25;
            this.label9.Text = "Number of JNB Projects to create:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.TXTNoJNBProjects);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(13, 19);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(281, 45);
            this.groupBox3.TabIndex = 26;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "JNB Projects";
            // 
            // TXTNoJNBProjects
            // 
            this.TXTNoJNBProjects.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TXTNoJNBProjects.Location = new System.Drawing.Point(216, 13);
            this.TXTNoJNBProjects.Name = "TXTNoJNBProjects";
            this.TXTNoJNBProjects.Size = new System.Drawing.Size(28, 20);
            this.TXTNoJNBProjects.TabIndex = 13;
            this.TXTNoJNBProjects.TextChanged += new System.EventHandler(this.TXTNoJNBProjects_TextChanged);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.TXTNoMMProjects);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(13, 70);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(281, 47);
            this.groupBox4.TabIndex = 27;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "MM Projects";
            // 
            // TXTNoMMProjects
            // 
            this.TXTNoMMProjects.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TXTNoMMProjects.Location = new System.Drawing.Point(216, 16);
            this.TXTNoMMProjects.Name = "TXTNoMMProjects";
            this.TXTNoMMProjects.Size = new System.Drawing.Size(28, 20);
            this.TXTNoMMProjects.TabIndex = 14;
            this.TXTNoMMProjects.TextChanged += new System.EventHandler(this.TXTNoMMProjects_TextChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(9, 19);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(166, 13);
            this.label10.TabIndex = 0;
            this.label10.Text = "Number of MM Projects to create:";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.TXTNoOfJBAProjects);
            this.groupBox5.Controls.Add(this.label11);
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.Location = new System.Drawing.Point(13, 130);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(281, 44);
            this.groupBox5.TabIndex = 28;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "JBA Projects";
            // 
            // TXTNoOfJBAProjects
            // 
            this.TXTNoOfJBAProjects.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TXTNoOfJBAProjects.Location = new System.Drawing.Point(216, 13);
            this.TXTNoOfJBAProjects.Name = "TXTNoOfJBAProjects";
            this.TXTNoOfJBAProjects.Size = new System.Drawing.Size(28, 20);
            this.TXTNoOfJBAProjects.TabIndex = 15;
            this.TXTNoOfJBAProjects.TextChanged += new System.EventHandler(this.TXTNoOfJBAProjects_TextChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(6, 16);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(162, 13);
            this.label11.TabIndex = 0;
            this.label11.Text = "Number of JBA Project to create:";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.groupBox5);
            this.groupBox6.Controls.Add(this.groupBox3);
            this.groupBox6.Controls.Add(this.groupBox4);
            this.groupBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox6.Location = new System.Drawing.Point(24, 366);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(312, 188);
            this.groupBox6.TabIndex = 29;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Projects To Create";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Project_Copy.Properties.Resources.Bentley;
            this.pictureBox1.Location = new System.Drawing.Point(500, 26);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(131, 28);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 30;
            this.pictureBox1.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(22, 148);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(329, 13);
            this.label5.TabIndex = 31;
            this.label5.Text = "Select the Company you wish to copy the Framework Contract from?";
            // 
            // CBOFCCompany
            // 
            this.CBOFCCompany.FormattingEnabled = true;
            this.CBOFCCompany.Location = new System.Drawing.Point(365, 145);
            this.CBOFCCompany.Name = "CBOFCCompany";
            this.CBOFCCompany.Size = new System.Drawing.Size(210, 21);
            this.CBOFCCompany.TabIndex = 32;
            this.CBOFCCompany.SelectedIndexChanged += new System.EventHandler(this.CBOFCCompany_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(22, 221);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(153, 13);
            this.label7.TabIndex = 33;
            this.label7.Text = "New Framework Contract Title:";
            // 
            // TXTFCTitle
            // 
            this.TXTFCTitle.Location = new System.Drawing.Point(181, 218);
            this.TXTFCTitle.Name = "TXTFCTitle";
            this.TXTFCTitle.Size = new System.Drawing.Size(394, 20);
            this.TXTFCTitle.TabIndex = 8;
            this.TXTFCTitle.TextChanged += new System.EventHandler(this.TXTFCTitle_TextChanged);
            // 
            // FrmCopyProjectProposal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(643, 564);
            this.Controls.Add(this.TXTFCTitle);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.CBOFCCompany);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.TXTNewFrameWorkContract);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.TXTNewProjectCode);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.BTNExit);
            this.Controls.Add(this.BTNRoles);
            this.Controls.Add(this.CBOProjectProposal);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.CBOFrameworkContract);
            this.Controls.Add(this.CBOProjCompany);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FrmCopyProjectProposal";
            this.Text = "Copy a Framework Contract ";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox CBOProjCompany;
        private System.Windows.Forms.ComboBox CBOFrameworkContract;
        private System.Windows.Forms.RadioButton RBProject;
        private System.Windows.Forms.RadioButton RBProposal;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox CBOProjectProposal;
        private System.Windows.Forms.RadioButton RBCopyFrameWorkContract;
        private System.Windows.Forms.RadioButton RBCopyProject;
        private System.Windows.Forms.Button BTNRoles;
        private System.Windows.Forms.Button BTNExit;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TXTNewProjectCode;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox TXTNewFrameWorkContract;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox TXTNoJNBProjects;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox TXTNoMMProjects;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox TXTNoOfJBAProjects;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox CBOFCCompany;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox TXTFCTitle;
        private System.Windows.Forms.ToolTip TTCodeFormat;
    }
}

