﻿namespace Project_Copy
{
    partial class FrmDefineRoles
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.LSTPositions = new System.Windows.Forms.ListBox();
            this.LBLNewProjectCodes = new System.Windows.Forms.Label();
            this.LBLPositions = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.DGVPersonsInvolved = new System.Windows.Forms.DataGridView();
            this.LSTProject = new System.Windows.Forms.ListBox();
            this.GRPProjectDetails = new System.Windows.Forms.GroupBox();
            this.CHBPrincipleProject = new System.Windows.Forms.CheckBox();
            this.CBOState = new System.Windows.Forms.ComboBox();
            this.CBOCountry = new System.Windows.Forms.ComboBox();
            this.TXTPostCode = new System.Windows.Forms.TextBox();
            this.TXTAdd3 = new System.Windows.Forms.TextBox();
            this.TXTCity = new System.Windows.Forms.TextBox();
            this.TXTAdd2 = new System.Windows.Forms.TextBox();
            this.TXTAdd1 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.TXTProjectName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.BTN_CreateProjects = new System.Windows.Forms.Button();
            this.BTNCancel = new System.Windows.Forms.Button();
            this.ProjectNameValidator = new System.Windows.Forms.ErrorProvider(this.components);
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.AddressLine1Validator = new System.Windows.Forms.ErrorProvider(this.components);
            this.PostCodeValidator = new System.Windows.Forms.ErrorProvider(this.components);
            this.StateValidator = new System.Windows.Forms.ErrorProvider(this.components);
            this.CountryValidator = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.DGVPersonsInvolved)).BeginInit();
            this.GRPProjectDetails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ProjectNameValidator)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddressLine1Validator)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PostCodeValidator)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StateValidator)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CountryValidator)).BeginInit();
            this.SuspendLayout();
            // 
            // LSTPositions
            // 
            this.LSTPositions.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LSTPositions.FormattingEnabled = true;
            this.LSTPositions.Location = new System.Drawing.Point(27, 255);
            this.LSTPositions.Name = "LSTPositions";
            this.LSTPositions.Size = new System.Drawing.Size(241, 485);
            this.LSTPositions.TabIndex = 1;
            this.LSTPositions.SelectedIndexChanged += new System.EventHandler(this.LSTPositions_SelectedIndexChanged);
            // 
            // LBLNewProjectCodes
            // 
            this.LBLNewProjectCodes.AutoSize = true;
            this.LBLNewProjectCodes.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LBLNewProjectCodes.Location = new System.Drawing.Point(24, 38);
            this.LBLNewProjectCodes.Name = "LBLNewProjectCodes";
            this.LBLNewProjectCodes.Size = new System.Drawing.Size(119, 13);
            this.LBLNewProjectCodes.TabIndex = 3;
            this.LBLNewProjectCodes.Text = "New Project Codes:";
            // 
            // LBLPositions
            // 
            this.LBLPositions.AutoSize = true;
            this.LBLPositions.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LBLPositions.Location = new System.Drawing.Point(24, 239);
            this.LBLPositions.Name = "LBLPositions";
            this.LBLPositions.Size = new System.Drawing.Size(62, 13);
            this.LBLPositions.TabIndex = 4;
            this.LBLPositions.Text = "Positions:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(287, 239);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(181, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Persons Involved in selected project:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(567, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(334, 26);
            this.label1.TabIndex = 8;
            this.label1.Text = "Define Roles For Each Project";
            // 
            // DGVPersonsInvolved
            // 
            this.DGVPersonsInvolved.AllowUserToAddRows = false;
            this.DGVPersonsInvolved.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGVPersonsInvolved.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DGVPersonsInvolved.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGVPersonsInvolved.DefaultCellStyle = dataGridViewCellStyle2;
            this.DGVPersonsInvolved.Location = new System.Drawing.Point(290, 255);
            this.DGVPersonsInvolved.Name = "DGVPersonsInvolved";
            this.DGVPersonsInvolved.RowHeadersWidth = 51;
            this.DGVPersonsInvolved.Size = new System.Drawing.Size(1021, 485);
            this.DGVPersonsInvolved.TabIndex = 9;
            this.DGVPersonsInvolved.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGVPersonsInvolved_CellContentClick);
            this.DGVPersonsInvolved.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGVPersonsInvolved_CellValueChanged);
            // 
            // LSTProject
            // 
            this.LSTProject.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LSTProject.FormattingEnabled = true;
            this.LSTProject.Location = new System.Drawing.Point(27, 54);
            this.LSTProject.Name = "LSTProject";
            this.LSTProject.Size = new System.Drawing.Size(100, 173);
            this.LSTProject.TabIndex = 10;
            this.LSTProject.SelectedIndexChanged += new System.EventHandler(this.LSTProject_SelectedIndexChanged);
            // 
            // GRPProjectDetails
            // 
            this.GRPProjectDetails.Controls.Add(this.CHBPrincipleProject);
            this.GRPProjectDetails.Controls.Add(this.CBOState);
            this.GRPProjectDetails.Controls.Add(this.CBOCountry);
            this.GRPProjectDetails.Controls.Add(this.TXTPostCode);
            this.GRPProjectDetails.Controls.Add(this.TXTAdd3);
            this.GRPProjectDetails.Controls.Add(this.TXTCity);
            this.GRPProjectDetails.Controls.Add(this.TXTAdd2);
            this.GRPProjectDetails.Controls.Add(this.TXTAdd1);
            this.GRPProjectDetails.Controls.Add(this.label10);
            this.GRPProjectDetails.Controls.Add(this.label9);
            this.GRPProjectDetails.Controls.Add(this.label8);
            this.GRPProjectDetails.Controls.Add(this.label7);
            this.GRPProjectDetails.Controls.Add(this.label6);
            this.GRPProjectDetails.Controls.Add(this.label5);
            this.GRPProjectDetails.Controls.Add(this.label4);
            this.GRPProjectDetails.Controls.Add(this.TXTProjectName);
            this.GRPProjectDetails.Controls.Add(this.label2);
            this.GRPProjectDetails.Location = new System.Drawing.Point(290, 54);
            this.GRPProjectDetails.Name = "GRPProjectDetails";
            this.GRPProjectDetails.Size = new System.Drawing.Size(658, 153);
            this.GRPProjectDetails.TabIndex = 11;
            this.GRPProjectDetails.TabStop = false;
            this.GRPProjectDetails.Text = "Project Details";
            // 
            // CHBPrincipleProject
            // 
            this.CHBPrincipleProject.AutoSize = true;
            this.CHBPrincipleProject.Location = new System.Drawing.Point(418, 99);
            this.CHBPrincipleProject.Name = "CHBPrincipleProject";
            this.CHBPrincipleProject.Size = new System.Drawing.Size(102, 17);
            this.CHBPrincipleProject.TabIndex = 9;
            this.CHBPrincipleProject.Text = "Principal Project";
            this.CHBPrincipleProject.UseVisualStyleBackColor = true;
            // 
            // CBOState
            // 
            this.CBOState.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.CBOState.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CBOState.FormattingEnabled = true;
            this.CBOState.ItemHeight = 13;
            this.CBOState.Location = new System.Drawing.Point(541, 126);
            this.CBOState.Name = "CBOState";
            this.CBOState.Size = new System.Drawing.Size(104, 21);
            this.CBOState.TabIndex = 8;
            this.CBOState.Validating += new System.ComponentModel.CancelEventHandler(this.CBOState_Validating);
            // 
            // CBOCountry
            // 
            this.CBOCountry.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.CBOCountry.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CBOCountry.FormattingEnabled = true;
            this.CBOCountry.ItemHeight = 13;
            this.CBOCountry.Location = new System.Drawing.Point(382, 126);
            this.CBOCountry.Name = "CBOCountry";
            this.CBOCountry.Size = new System.Drawing.Size(104, 21);
            this.CBOCountry.TabIndex = 7;
            this.CBOCountry.SelectedIndexChanged += new System.EventHandler(this.CBOCountry_SelectedIndexChanged);
            this.CBOCountry.Validating += new System.ComponentModel.CancelEventHandler(this.CBOCountry_Validating);
            // 
            // TXTPostCode
            // 
            this.TXTPostCode.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.TXTPostCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TXTPostCode.Location = new System.Drawing.Point(236, 127);
            this.TXTPostCode.Name = "TXTPostCode";
            this.TXTPostCode.Size = new System.Drawing.Size(86, 20);
            this.TXTPostCode.TabIndex = 6;
            this.TXTPostCode.Validating += new System.ComponentModel.CancelEventHandler(this.TXTPostCode_Validating);
            // 
            // TXTAdd3
            // 
            this.TXTAdd3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TXTAdd3.Location = new System.Drawing.Point(53, 97);
            this.TXTAdd3.Name = "TXTAdd3";
            this.TXTAdd3.Size = new System.Drawing.Size(346, 20);
            this.TXTAdd3.TabIndex = 4;
            // 
            // TXTCity
            // 
            this.TXTCity.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TXTCity.Location = new System.Drawing.Point(53, 127);
            this.TXTCity.Name = "TXTCity";
            this.TXTCity.Size = new System.Drawing.Size(117, 20);
            this.TXTCity.TabIndex = 5;
            // 
            // TXTAdd2
            // 
            this.TXTAdd2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TXTAdd2.Location = new System.Drawing.Point(53, 72);
            this.TXTAdd2.Name = "TXTAdd2";
            this.TXTAdd2.Size = new System.Drawing.Size(346, 20);
            this.TXTAdd2.TabIndex = 3;
            // 
            // TXTAdd1
            // 
            this.TXTAdd1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.TXTAdd1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TXTAdd1.Location = new System.Drawing.Point(53, 46);
            this.TXTAdd1.Name = "TXTAdd1";
            this.TXTAdd1.Size = new System.Drawing.Size(346, 20);
            this.TXTAdd1.TabIndex = 2;
            this.TXTAdd1.Validating += new System.ComponentModel.CancelEventHandler(this.TXT_Add1Validating);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(504, 130);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(32, 13);
            this.label10.TabIndex = 8;
            this.label10.Text = "State";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(334, 131);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(43, 13);
            this.label9.TabIndex = 7;
            this.label9.Text = "Country";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(175, 130);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(56, 13);
            this.label8.TabIndex = 6;
            this.label8.Text = "Post Code";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(13, 130);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(24, 13);
            this.label7.TabIndex = 5;
            this.label7.Text = "City";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(12, 100);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(36, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "Line 3";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(13, 75);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(36, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "Line 2";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 49);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(36, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Line 1";
            // 
            // TXTProjectName
            // 
            this.TXTProjectName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.TXTProjectName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TXTProjectName.Location = new System.Drawing.Point(53, 23);
            this.TXTProjectName.Name = "TXTProjectName";
            this.TXTProjectName.Size = new System.Drawing.Size(572, 20);
            this.TXTProjectName.TabIndex = 1;
            this.TXTProjectName.Validating += new System.ComponentModel.CancelEventHandler(this.TXTProjectName_Validating);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Name";
            // 
            // BTN_CreateProjects
            // 
            this.BTN_CreateProjects.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTN_CreateProjects.Location = new System.Drawing.Point(96, 762);
            this.BTN_CreateProjects.Name = "BTN_CreateProjects";
            this.BTN_CreateProjects.Size = new System.Drawing.Size(93, 23);
            this.BTN_CreateProjects.TabIndex = 12;
            this.BTN_CreateProjects.Text = "Create Projects";
            this.BTN_CreateProjects.UseVisualStyleBackColor = true;
            this.BTN_CreateProjects.Click += new System.EventHandler(this.BTN_CreateProjects_Click);
            // 
            // BTNCancel
            // 
            this.BTNCancel.CausesValidation = false;
            this.BTNCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BTNCancel.Location = new System.Drawing.Point(27, 762);
            this.BTNCancel.Name = "BTNCancel";
            this.BTNCancel.Size = new System.Drawing.Size(64, 23);
            this.BTNCancel.TabIndex = 13;
            this.BTNCancel.Text = "Cancel";
            this.BTNCancel.UseVisualStyleBackColor = true;
            this.BTNCancel.Click += new System.EventHandler(this.BTNCancel_Click);
            // 
            // ProjectNameValidator
            // 
            this.ProjectNameValidator.ContainerControl = this;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Project_Copy.Properties.Resources.Bentley;
            this.pictureBox1.Location = new System.Drawing.Point(1198, 9);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(112, 28);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 31;
            this.pictureBox1.TabStop = false;
            // 
            // AddressLine1Validator
            // 
            this.AddressLine1Validator.ContainerControl = this;
            // 
            // PostCodeValidator
            // 
            this.PostCodeValidator.ContainerControl = this;
            // 
            // StateValidator
            // 
            this.StateValidator.ContainerControl = this;
            // 
            // CountryValidator
            // 
            this.CountryValidator.ContainerControl = this;
            // 
            // FrmDefineRoles
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CausesValidation = false;
            this.ClientSize = new System.Drawing.Size(1323, 791);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.BTNCancel);
            this.Controls.Add(this.BTN_CreateProjects);
            this.Controls.Add(this.GRPProjectDetails);
            this.Controls.Add(this.LSTProject);
            this.Controls.Add(this.DGVPersonsInvolved);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.LBLPositions);
            this.Controls.Add(this.LBLNewProjectCodes);
            this.Controls.Add(this.LSTPositions);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "FrmDefineRoles";
            this.Text = "Define Roles For Each Project";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.Validating += new System.ComponentModel.CancelEventHandler(this.TXT_Add1Validating);
            ((System.ComponentModel.ISupportInitialize)(this.DGVPersonsInvolved)).EndInit();
            this.GRPProjectDetails.ResumeLayout(false);
            this.GRPProjectDetails.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ProjectNameValidator)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddressLine1Validator)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PostCodeValidator)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StateValidator)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CountryValidator)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ListBox LSTPositions;
        private System.Windows.Forms.Label LBLNewProjectCodes;
        private System.Windows.Forms.Label LBLPositions;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView DGVPersonsInvolved;
        private System.Windows.Forms.ListBox LSTProject;
        private System.Windows.Forms.GroupBox GRPProjectDetails;
        private System.Windows.Forms.ComboBox CBOState;
        private System.Windows.Forms.ComboBox CBOCountry;
        private System.Windows.Forms.TextBox TXTPostCode;
        private System.Windows.Forms.TextBox TXTAdd3;
        private System.Windows.Forms.TextBox TXTCity;
        private System.Windows.Forms.TextBox TXTAdd2;
        private System.Windows.Forms.TextBox TXTAdd1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox TXTProjectName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button BTN_CreateProjects;
        private System.Windows.Forms.Button BTNCancel;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.CheckBox CHBPrincipleProject;
        private System.Windows.Forms.ErrorProvider ProjectNameValidator;
        private System.Windows.Forms.ErrorProvider AddressLine1Validator;
        private System.Windows.Forms.ErrorProvider PostCodeValidator;
        private System.Windows.Forms.ErrorProvider StateValidator;
        private System.Windows.Forms.ErrorProvider CountryValidator;
    }
}