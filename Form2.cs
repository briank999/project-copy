﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using System.Drawing;

namespace Project_Copy
{
    public partial class FrmDefineRoles : Form
    {
        private int iNoOfProjectProposals;
        private string sSourceProjectCode;
        private string sTargetProjectCode;
        private bool isInitialised = false;
        private String sSelectedProjectCode;
        private ProjectDetails SelectedProject;
        private ProjectAddressDetails SelectedProjectAddressDetails;
        private PositionDetails SelectedProjectPositions;
        private string sUserName;
        private string sSelectedPosition;
        private string sProjCompCode;
        private string sFCCompCode;
        private int iSelectedPosition;
        public string sConnectionString;
        public string sXpConnectionString;
        public string sOriginator;
        public string sProjType;
        public string sSourceFrameworkContract;
        public string sTargetFrameworkContract;
        public int iJNBProjects;
        public int iMMProjects;
        public int iJBAProjects;
        public bool bAddressLine1Entered;
        public bool bPostCodeEntered;
        public bool bNameEntered;
        public bool bCountryEntered;
        public bool bStateEntered;
        public string sFrameworkContractTitle;

        public FrmDefineRoles()
        {
            InitializeComponent();
        }

        public FrmDefineRoles(int iNoOfProjProps, 
            string sSourceFC, 
            string sTargetFC, 
            string sTargetProjCode, 
            string sSourceProjCode, 
            string sProjCompany, 
            string sFCCompany,
            string sProjectType, 
            int iJNBProj, 
            int iMMProj, 
            int iJBAProj,
            string sFCTitle)
        {
            this.sSourceProjectCode = sSourceProjCode;
            this.sTargetProjectCode = sTargetProjCode;
            this.sSourceFrameworkContract = sSourceFC;
            this.sTargetFrameworkContract = sTargetFC;
            this.sProjCompCode = sProjCompany;
            this.sFCCompCode = sFCCompany;
            this.sProjType = sProjectType;
            this.iJNBProjects = iJNBProj;
            this.iMMProjects = iMMProj;
            this.iJBAProjects = iJBAProj;
            this.sFrameworkContractTitle = sFCTitle;
            InitializeComponent();
            sConnectionString = Properties.Settings.Default.ExternalDataConnectionString;
            sOriginator = Environment.UserDomainName + "\\" + Environment.UserName;
            sXpConnectionString = Properties.Settings.Default.XpedeonConnectionString;
        }
        void RefreshState()
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = sConnectionString;
                conn.Open();

                SqlCommand cmd = new SqlCommand("Utility.GetStates", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter CountryParameter = new SqlParameter("@sCountryCode", SqlDbType.Char);
                CountryParameter.Value = CBOCountry.SelectedValue.ToString();
                cmd.Parameters.Add(CountryParameter);

                SqlDataAdapter da = new SqlDataAdapter(cmd);

                DataTable dt = new DataTable();

                da.Fill(dt);

                BindingSource bs1 = new BindingSource();

                bs1.DataSource = dt;

                CBOState.DataSource = bs1;

                CBOState.ResetText();

                CBOState.DisplayMember = "State_Name";
                CBOState.ValueMember = "State_Code";
            }
        }
        void CreateProjects()
        {
            using (SqlConnection conn = new SqlConnection())
            {
//              SqlTransaction trans = null;
                conn.ConnectionString = sXpConnectionString;
                conn.Open();

//                trans = conn.BeginTransaction();

                SqlCommand cmd = new SqlCommand("Utility.CreateProject", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter OrigParameter = new SqlParameter("@Originator", SqlDbType.Char);
                OrigParameter.Value = sOriginator;
                cmd.Parameters.Add(OrigParameter);

                SqlParameter SourceFCParameter = new SqlParameter("@SourceFrameworkContract", SqlDbType.Char);
                SourceFCParameter.Value = sSourceFrameworkContract;
                cmd.Parameters.Add(SourceFCParameter);

                SqlParameter TargetFCParameter = new SqlParameter("@TargetFrameworkContract", SqlDbType.Char);
                TargetFCParameter.Value = sTargetFrameworkContract;
                cmd.Parameters.Add(TargetFCParameter);

                SqlParameter ProjectTypeParameter = new SqlParameter("@ProjectType", SqlDbType.Char);
                ProjectTypeParameter.Value = sProjType;
                cmd.Parameters.Add(ProjectTypeParameter);

                SqlParameter SourceProjParameter = new SqlParameter("@SourceProject", SqlDbType.Char);
                SourceProjParameter.Value = sSourceProjectCode;
                cmd.Parameters.Add(SourceProjParameter);

                SqlParameter ProjCompParameter = new SqlParameter("@ProjCompanyCode", SqlDbType.Char);
                ProjCompParameter.Value = sProjCompCode;
                cmd.Parameters.Add(ProjCompParameter);

                SqlParameter FCCompParameter = new SqlParameter("@FCCompanyCode", SqlDbType.Char);
                FCCompParameter.Value = sFCCompCode;
                cmd.Parameters.Add(FCCompParameter);

                SqlParameter NoJNBParameter = new SqlParameter("@iNoJNBProjects", SqlDbType.Int);
                NoJNBParameter.Value = iJNBProjects;
                cmd.Parameters.Add(NoJNBParameter);

                SqlParameter NoMMParameter = new SqlParameter("@iNoMMProjects", SqlDbType.Int);
                NoMMParameter.Value = iMMProjects;
                cmd.Parameters.Add(NoMMParameter);

                SqlParameter NoJBAParameter = new SqlParameter("@iNoJBAProjects", SqlDbType.Int);
                NoJBAParameter.Value = iJBAProjects;
                cmd.Parameters.Add(NoJBAParameter);

                SqlParameter FCTitleParameter = new SqlParameter("@FrameworkContractTitle", SqlDbType.VarChar);
                FCTitleParameter.Value = sFrameworkContractTitle;
                cmd.Parameters.Add(FCTitleParameter);

                SqlParameter CreationDetails = new SqlParameter("@CreatedDetails", SqlDbType.VarChar);
                CreationDetails.Direction = ParameterDirection.Output;
                CreationDetails.Size = 500;
                cmd.Parameters.Add(CreationDetails);

                try
                {
                    cmd.ExecuteNonQuery();

//                    trans.Commit();

                    string message = "The projects have now been created." + CreationDetails.Value.ToString();
                    string caption = "Project Creation";
                    MessageBoxButtons buttons = MessageBoxButtons.OK;
                    DialogResult result;

                    result = MessageBox.Show(message, caption, buttons);

                    sConnectionString = Properties.Settings.Default.ExternalDataConnectionString;
                    sOriginator = Environment.UserDomainName + "\\" + Environment.UserName;

                    using (SqlConnection conn1 = new SqlConnection())
                    {
                        conn1.ConnectionString = sConnectionString;
                        conn1.Open();

                        SqlCommand cmd1 = new SqlCommand("Utility.PrjCpyCleanup", conn1);
                        cmd1.CommandType = CommandType.StoredProcedure;

                        SqlParameter OriginatorParameter = new SqlParameter("@Originator", SqlDbType.Char);
                        OriginatorParameter.Value = sOriginator;
                        cmd1.Parameters.Add(OriginatorParameter);

                        cmd1.ExecuteNonQuery();
                        conn1.Close();

                        this.Close();
                    }
                }
                catch (SqlException ex)
                {
                    string message = "There was an error while creating the projects. Please contact IT. The error is shown below:\n\n";
                    message = string.Concat(message, ex.Message.ToString());

                    string caption = "Project Creation";
                    MessageBoxButtons buttons = MessageBoxButtons.OK;
                    DialogResult result;

                    result = MessageBox.Show(message, caption, buttons);
                }
                finally
                {
                    cmd.Dispose();
                    conn.Close();
                }
            }
        }
        void InitialiseDataGridColumns()
        {
            DataGridViewCheckBoxColumn SelectedCheckBox = new DataGridViewCheckBoxColumn();
            SelectedCheckBox.HeaderText = "Selected";
            SelectedCheckBox.Name = "Selected";
            SelectedCheckBox.DefaultCellStyle.Font = new Font(DataGridView.DefaultFont, FontStyle.Regular);
            SelectedCheckBox.Width = 60;
            SelectedCheckBox.SortMode = DataGridViewColumnSortMode.NotSortable;
            DGVPersonsInvolved.Columns.Add(SelectedCheckBox);

            DataGridViewCheckBoxColumn PrimaryCheckBox = new DataGridViewCheckBoxColumn();
            PrimaryCheckBox.HeaderText = "Primary";
            PrimaryCheckBox.Name = "Primary";
            PrimaryCheckBox.Width = 50;
            PrimaryCheckBox.SortMode = DataGridViewColumnSortMode.NotSortable;
            DGVPersonsInvolved.Columns.Add(PrimaryCheckBox);

            DataGridViewTextBoxColumn UserTextBox = new DataGridViewTextBoxColumn();
            UserTextBox.HeaderText = "User";
            UserTextBox.Name = "User";
            UserTextBox.DataPropertyName = "User";
            UserTextBox.DefaultCellStyle.Font = new Font(DataGridView.DefaultFont, FontStyle.Regular);
            UserTextBox.Width = 150;
            UserTextBox.SortMode = DataGridViewColumnSortMode.NotSortable;
            UserTextBox.ReadOnly = true;
            DGVPersonsInvolved.Columns.Add(UserTextBox);

            DataGridViewTextBoxColumn AccessTextBox = new DataGridViewTextBoxColumn();
            AccessTextBox.HeaderText = "Access";
            AccessTextBox.Name = "Access";
            AccessTextBox.DataPropertyName = "Access";
            AccessTextBox.DefaultCellStyle.Font = new Font(DataGridView.DefaultFont, FontStyle.Regular);
            AccessTextBox.Width = 100;
            AccessTextBox.SortMode = DataGridViewColumnSortMode.NotSortable;
            AccessTextBox.ReadOnly = true;
            DGVPersonsInvolved.Columns.Add(AccessTextBox);

            DataGridViewTextBoxColumn RemarksTextBox = new DataGridViewTextBoxColumn();
            RemarksTextBox.HeaderText = "Remarks";
            RemarksTextBox.Name = "Remarks";
            RemarksTextBox.DataPropertyName = "Remarks";
            RemarksTextBox.DefaultCellStyle.Font = new Font(DataGridView.DefaultFont, FontStyle.Regular);
            RemarksTextBox.Width = 100;
            RemarksTextBox.SortMode = DataGridViewColumnSortMode.NotSortable;
            RemarksTextBox.ReadOnly = true;
            DGVPersonsInvolved.Columns.Add(RemarksTextBox);

            DataGridViewTextBoxColumn EmailTextBox = new DataGridViewTextBoxColumn();
            EmailTextBox.HeaderText = "Email";
            EmailTextBox.Name = "Email";
            EmailTextBox.DataPropertyName = "Email";
            EmailTextBox.DefaultCellStyle.Font = new Font(DataGridView.DefaultFont, FontStyle.Regular);
            EmailTextBox.Width = 250;
            EmailTextBox.SortMode = DataGridViewColumnSortMode.NotSortable;
            EmailTextBox.ReadOnly = true;
            DGVPersonsInvolved.Columns.Add(EmailTextBox);

            DataGridViewTextBoxColumn EmployeeCodeTextBox = new DataGridViewTextBoxColumn();
            EmployeeCodeTextBox.HeaderText = "Employee Code";
            EmployeeCodeTextBox.Name = "EmployeeCode";
            EmployeeCodeTextBox.DataPropertyName = "EmployeeCode";
            EmployeeCodeTextBox.DefaultCellStyle.Font = new Font(DataGridView.DefaultFont, FontStyle.Regular);
            EmployeeCodeTextBox.Width = 60;
            EmployeeCodeTextBox.SortMode = DataGridViewColumnSortMode.NotSortable;
            EmployeeCodeTextBox.ReadOnly = true;
            DGVPersonsInvolved.Columns.Add(EmployeeCodeTextBox);

            DataGridViewTextBoxColumn EmployeeNameTextBox = new DataGridViewTextBoxColumn();
            EmployeeNameTextBox.HeaderText = "Employee Name";
            EmployeeNameTextBox.Name = "EmployeeName";
            EmployeeNameTextBox.DataPropertyName = "Division_name";
            EmployeeNameTextBox.DefaultCellStyle.Font = new Font(DataGridView.DefaultFont, FontStyle.Regular);
            EmployeeNameTextBox.Width = 250;
            EmployeeNameTextBox.SortMode = DataGridViewColumnSortMode.NotSortable;
            EmployeeNameTextBox.ReadOnly = true;
            DGVPersonsInvolved.Columns.Add(EmployeeNameTextBox);

            DataGridViewTextBoxColumn ActiveTextBox = new DataGridViewTextBoxColumn();
            ActiveTextBox.HeaderText = "Active";
            ActiveTextBox.Name = "Active";
            ActiveTextBox.DataPropertyName = "Active";
            ActiveTextBox.DefaultCellStyle.Font = new Font(DataGridView.DefaultFont, FontStyle.Regular);
            ActiveTextBox.Width = 50;
            ActiveTextBox.SortMode = DataGridViewColumnSortMode.NotSortable;
            ActiveTextBox.ReadOnly = true;
            DGVPersonsInvolved.Columns.Add(ActiveTextBox);

//This column is in the data set returned, but it does not need to be shown in the grid.
            DataGridViewTextBoxColumn EntityDivisionID = new DataGridViewTextBoxColumn();
            EntityDivisionID.Name = "DivisionID";
            EntityDivisionID.DataPropertyName = "Entity_Division_ID";
            EntityDivisionID.Visible = false;
            DGVPersonsInvolved.Columns.Add(EntityDivisionID);

            DataGridViewTextBoxColumn ContactID = new DataGridViewTextBoxColumn();
            ContactID.Name = "ContactID";
            ContactID.DataPropertyName = "Contact_ID";
            ContactID.Visible = false;
            DGVPersonsInvolved.Columns.Add(ContactID);
        }
        void ValidateAndEnable()
        {
            bAddressLine1Entered = false;
            bPostCodeEntered = false;
            bNameEntered = false;
            bCountryEntered = false;
            bStateEntered = false;
            if (!string.IsNullOrEmpty(TXTProjectName.Text))
            {
                bNameEntered = true;
            }
            if (!string.IsNullOrEmpty(TXTAdd1.Text))
            {
                bAddressLine1Entered = true;
            }
            if (!string.IsNullOrEmpty(TXTPostCode.Text))
            {
                bPostCodeEntered = true;
            }
            if (CBOCountry.SelectedIndex > - 1)
            {
                bCountryEntered = true;
            }
            if (CBOState.SelectedIndex > -1)
            {
                bStateEntered = true;
            }
            if (bNameEntered && bAddressLine1Entered && bPostCodeEntered && bCountryEntered && bStateEntered)
            {
                LSTProject.Enabled = true;
            }
            else
            {
                LSTProject.Enabled = false;
            }
        }
        void PopulateProjectList()
        {
            int iCount = 0;
            string sProjectCodeToAdd;
            string sStartValue = "";
            string sTemplateValue;
            int iEndCode;
            int iResult;

// Check that we are going to be able to create the projects requested for the template for JNB, MM and JBA.

            if (iJBAProjects > 0)
            {
                using (SqlConnection conn = new SqlConnection())
                {
                    conn.ConnectionString = sConnectionString;
                    conn.Open();

                    SqlCommand cmd = new SqlCommand("Utility.FindNextProjectCode", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter PatternParameter = new SqlParameter("@sPatternValue", SqlDbType.Char);
                    PatternParameter.Value = sTargetProjectCode;
                    cmd.Parameters.Add(PatternParameter);

                    SqlParameter HowManyParameter = new SqlParameter("@iHowMany", SqlDbType.Int);
                    HowManyParameter.Value = iJBAProjects;
                    cmd.Parameters.Add(HowManyParameter);

                    SqlParameter StartParameter = new SqlParameter("@sReturnValue", SqlDbType.VarChar);
                    StartParameter.Value = sStartValue;
                    StartParameter.Size = 7;
                    StartParameter.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(StartParameter);

                    cmd.ExecuteNonQuery();

                    sStartValue = cmd.Parameters["@sReturnValue"].Value.ToString();

                    cmd.Dispose();
                    conn.Close();
                }
            }

            if (iMMProjects > 0)
            {
                using (SqlConnection conn = new SqlConnection())
                {
                    conn.ConnectionString = sConnectionString;
                    conn.Open();

                    SqlCommand cmd = new SqlCommand("Utility.FindNextProjectCode", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter PatternParameter = new SqlParameter("@sPatternValue", SqlDbType.Char);
                    PatternParameter.Value = sTargetProjectCode;
                    cmd.Parameters.Add(PatternParameter);

                    SqlParameter HowManyParameter = new SqlParameter("@iHowMany", SqlDbType.Int);
                    HowManyParameter.Value = iMMProjects;
                    cmd.Parameters.Add(HowManyParameter);

                    SqlParameter StartParameter = new SqlParameter("@sReturnValue", SqlDbType.VarChar);
                    StartParameter.Value = sStartValue;
                    StartParameter.Size = 7;
                    StartParameter.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(StartParameter);

                    cmd.ExecuteNonQuery();

                    sStartValue = cmd.Parameters["@sReturnValue"].Value.ToString();

                    cmd.Dispose();
                    conn.Close();
                }
            }

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = sConnectionString;
                conn.Open();

                SqlCommand cmd = new SqlCommand("Utility.FindNextProjectCode", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter PatternParameter = new SqlParameter("@sPatternValue", SqlDbType.Char);
                PatternParameter.Value = sTargetProjectCode;
                cmd.Parameters.Add(PatternParameter);

                SqlParameter HowManyParameter = new SqlParameter("@iHowMany", SqlDbType.Int);
                HowManyParameter.Value = iJNBProjects;
                cmd.Parameters.Add(HowManyParameter);

                SqlParameter StartParameter = new SqlParameter("@sReturnValue", SqlDbType.VarChar);
                StartParameter.Direction = ParameterDirection.Output;
                StartParameter.Size = 7;
                cmd.Parameters.Add(StartParameter);

                SqlParameter RetValParameter = new SqlParameter("@return_value", SqlDbType.Int);
                RetValParameter.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(RetValParameter);

                cmd.ExecuteNonQuery();

                iResult = (int)cmd.Parameters["@return_value"].Value;
//Get the value returned from the SP.
                sStartValue = cmd.Parameters["@sReturnValue"].Value.ToString();

                if (iResult != 0)
                {
                    MessageBox.Show("There was a problem trying to determine the starting Project Code", "Project Numbering", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                cmd.Dispose();
                conn.Close();
            }

            DataTable table = new DataTable();

            table.Columns.Add("Project Code", typeof(string));

            iNoOfProjectProposals = iJNBProjects;

            if (iJBAProjects > iJNBProjects)
            {
                iNoOfProjectProposals = iJBAProjects;
            }

            if (iMMProjects > iJNBProjects)
            {
                iNoOfProjectProposals = iMMProjects;
            }

            sTemplateValue = sStartValue.Substring(0, sStartValue.Length - 2);
            iEndCode = Convert.ToInt32(sStartValue.Substring(sStartValue.Length - 2, 2));
            while (iCount < iNoOfProjectProposals)
            {
                iCount++;
                sProjectCodeToAdd = sTemplateValue + iEndCode.ToString("00");

                SelectedProject = new ProjectDetails();

                SelectedProject.PrincipleProject = false;
                SelectedProject.ProjectName = "";

                SelectedProject.AddNewProject(sProjectCodeToAdd, sConnectionString, sOriginator);

                table.Rows.Add(sProjectCodeToAdd);
                iEndCode++;
            }
            LSTProject.DataSource = table;
            LSTProject.DisplayMember = "Project Code";
            LSTProject.ValueMember = "Project Code";
            LSTProject.SelectedIndex = LSTProject.TopIndex;

            //Copy the source project positions into the application tables.
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = sConnectionString;
                conn.Open();

                SqlCommand cmd = new SqlCommand("Utility.PrjCpyCopyProjectPos", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter OrigParameter = new SqlParameter("@Originator", SqlDbType.Char);
                OrigParameter.Value = sOriginator;
                cmd.Parameters.Add(OrigParameter);

                SqlParameter CompParameter = new SqlParameter("@CompanyCode", SqlDbType.Char);
                CompParameter.Value = sProjCompCode;
                cmd.Parameters.Add(CompParameter);

                SqlParameter ProjParameter = new SqlParameter("@ProjectCode", SqlDbType.Char);
                ProjParameter.Value = sSourceProjectCode;
                cmd.Parameters.Add(ProjParameter);

                cmd.ExecuteNonQuery();
            }
        }
        void UpdateProjectDetails()
        {
            if (CBOCountry.SelectedValue.ToString() == "UK")
            {
                if (CBOState.SelectedIndex == -1)
                {
                    MessageBox.Show("Please select a state.", "Project validation", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }

            if (SelectedProject.bCreateProjectRecord)
            {
                SelectedProject.ProjectName = TXTProjectName.Text;
                if (CHBPrincipleProject.Checked)
                {
                    SelectedProject.PrincipleProject = true;
                }
                else
                {
                    SelectedProject.PrincipleProject = false;
                }
                SelectedProject.AddNewProject(sSelectedProjectCode, sConnectionString, sOriginator);
            }
            else
            {
                SelectedProject.ProjectName = TXTProjectName.Text;
                if (CHBPrincipleProject.Checked)
                {
                    SelectedProject.PrincipleProject = true;
                }
                else
                {
                    SelectedProject.PrincipleProject = false;
                }
                SelectedProject.UpdateProject(sSelectedProjectCode, sConnectionString, sOriginator);
            }

            if (SelectedProjectAddressDetails.bCreateAddressRecord)
            {
                SelectedProjectAddressDetails.Add1 = TXTAdd1.Text;
                SelectedProjectAddressDetails.Add2 = TXTAdd2.Text;
                SelectedProjectAddressDetails.Add3 = TXTAdd3.Text;
                SelectedProjectAddressDetails.City = TXTCity.Text;
                SelectedProjectAddressDetails.PostCode = TXTPostCode.Text;
                if (Convert.ToInt32(CBOState.Items.Count.ToString()) > 0)
                {
                    SelectedProjectAddressDetails.State = CBOState.SelectedValue.ToString();
                }
                else
                {
                    SelectedProjectAddressDetails.State = "";
                }

                if (Convert.ToInt32(CBOCountry.Items.Count.ToString()) > 0)
                {
                    SelectedProjectAddressDetails.CountryCode = CBOCountry.SelectedValue.ToString();
                }
                else
                {
                    SelectedProjectAddressDetails.CountryCode = "";
                }
                SelectedProjectAddressDetails.sProjCode = sSelectedProjectCode;
                SelectedProjectAddressDetails.sOrig = sOriginator;

                SelectedProjectAddressDetails.AddNewAddress(sConnectionString);
            }
            else
            {
                SelectedProjectAddressDetails.Add1 = TXTAdd1.Text;
                SelectedProjectAddressDetails.Add2 = TXTAdd2.Text;
                SelectedProjectAddressDetails.Add3 = TXTAdd3.Text;
                SelectedProjectAddressDetails.City = TXTCity.Text;
                SelectedProjectAddressDetails.PostCode = TXTPostCode.Text;
                if (Convert.ToInt32(CBOState.Items.Count.ToString()) > 0)
                {
                    SelectedProjectAddressDetails.State = CBOState.SelectedValue.ToString();
                }
                else
                {
                    SelectedProjectAddressDetails.State = "";
                }

                if (Convert.ToInt32(CBOCountry.Items.Count.ToString()) > 0)
                {
                    SelectedProjectAddressDetails.CountryCode = CBOCountry.SelectedValue.ToString();
                }
                else
                {
                    SelectedProjectAddressDetails.CountryCode = "";
                }

                SelectedProjectAddressDetails.sProjCode = sSelectedProjectCode;
                SelectedProjectAddressDetails.sOrig = sOriginator;

                SelectedProjectAddressDetails.UpdateAddress(sConnectionString);
            }
        }
        void UpdatePosIndDetails()
        {
            foreach (DataGridViewRow row in DGVPersonsInvolved.Rows)
            {
                SelectedProjectPositions = new PositionDetails();

                DataGridViewTextBoxCell oUser;
                oUser = row.Cells["User"] as DataGridViewTextBoxCell;
                SelectedProjectPositions.sPerson = oUser.Value.ToString();
                SelectedProjectPositions.sProjectCode = sSelectedProjectCode;
                SelectedProjectPositions.iPos = iSelectedPosition;
                SelectedProjectPositions.sOrig = sOriginator;

                SelectedProjectPositions.DelPersonPosition(sConnectionString);

                DataGridViewCheckBoxCell oSelected;
                oSelected = (DataGridViewCheckBoxCell)row.Cells["Selected"];

                if (oSelected == null)
                    {
                    continue;
                }

                if (oSelected.Value == null)
                {
                    continue;
                }

                if ((bool)oSelected.Value == false)
                {
                    continue;
                }

                DataGridViewCheckBoxCell oPrimary;
                oPrimary = (DataGridViewCheckBoxCell)row.Cells["Primary"];

                if (oPrimary == null)
                {
                    SelectedProjectPositions.bIsPrimary = false;
                }

                if (oPrimary.Value == null)
                {
                    SelectedProjectPositions.bIsPrimary = false;
                }
                else
                {
                    if ((bool)oPrimary.Value == true)
                    {
                        SelectedProjectPositions.bIsPrimary = true;
                    }
                    else
                    {
                        SelectedProjectPositions.bIsPrimary = false;
                    }
                }
                DataGridViewTextBoxCell oIdentityDivisionID;
                oIdentityDivisionID = row.Cells["DivisionID"] as DataGridViewTextBoxCell;
                SelectedProjectPositions.iIdentity = Convert.ToInt32(oIdentityDivisionID.Value.ToString());

                DataGridViewTextBoxCell oContactID;
                oContactID = row.Cells["ContactID"] as DataGridViewTextBoxCell;
                SelectedProjectPositions.iContact = Convert.ToInt32(oContactID.Value.ToString());

                SelectedProjectPositions.sCompanyCode = sProjCompCode;

                SelectedProjectPositions.AddPersonPosition(sConnectionString);
            }
        }
        void GetPersonsAllowedToBeInvolved()
        {
            //Populate the positions grid
            DGVPersonsInvolved.AutoGenerateColumns = false;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = sConnectionString;
                conn.Open();

                SqlCommand cmd = new SqlCommand("Utility.GetPersonsAllowedToBeInvolved", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter CompanyParameter = new SqlParameter("@CompanyCode", SqlDbType.Char);
                CompanyParameter.Value = "JNB";

                cmd.Parameters.Add(CompanyParameter);

                SqlParameter PosIDParameter = new SqlParameter("@PosID", SqlDbType.Int);
                PosIDParameter.Value = LSTPositions.SelectedValue;

                cmd.Parameters.Add(PosIDParameter);

                SqlDataAdapter da = new SqlDataAdapter(cmd);

                DataTable dt = new DataTable();

                da.Fill(dt);

                BindingSource bs1 = new BindingSource();
                bs1.DataSource = dt;
                DGVPersonsInvolved.DataSource = bs1;
            }
        }
        void GetSelectedPosInd()
        {
            int iIndex = 0;
            bool bFoundRecord;

            foreach (DataGridViewRow row in DGVPersonsInvolved.Rows)
            {
                DataGridViewTextBoxCell oCell;
                oCell = row.Cells["User"] as DataGridViewTextBoxCell;
                sUserName = oCell.Value.ToString();

                SelectedProjectPositions = new PositionDetails();
                SelectedProjectPositions.iPos = iSelectedPosition;
                SelectedProjectPositions.sPerson = sUserName;
                SelectedProjectPositions.sProjectCode = sSelectedProjectCode;
                SelectedProjectPositions.sOrig = sOriginator;

                bFoundRecord = SelectedProjectPositions.GetPersonPosition(sConnectionString);
                if (bFoundRecord)
                {
                    DGVPersonsInvolved.Rows[iIndex].Cells["Selected"].Value = true;
                    if (SelectedProjectPositions.bIsPrimary)
                    {
                        DGVPersonsInvolved.Rows[iIndex].Cells["Primary"].Value = true;
                    }
                    DGVPersonsInvolved.Rows[iIndex].Cells["DivisionID"].Value = SelectedProjectPositions.iIdentity;
                    DGVPersonsInvolved.Rows[iIndex].Cells["ContactID"].Value = SelectedProjectPositions.iContact;
                }
                iIndex++;
            }
        }
        void PopulateProjectDetails()
        {
            GRPProjectDetails.Text = string.Concat("Project Selected: ", LSTProject.SelectedValue);

            SelectedProject = new ProjectDetails();
            SelectedProject.GetProject(LSTProject.SelectedValue.ToString(),sConnectionString, sOriginator);

            TXTProjectName.Text = SelectedProject.ProjectName;
            if (SelectedProject.PrincipleProject)
            {
                CHBPrincipleProject.Checked = true;
            }
            else
            {
                CHBPrincipleProject.Checked = false;
            }

            SelectedProjectAddressDetails = new ProjectAddressDetails();
            SelectedProjectAddressDetails.sProjCode = LSTProject.SelectedValue.ToString();
            SelectedProjectAddressDetails.sOrig = sOriginator;
            SelectedProjectAddressDetails.GetAddress(sConnectionString);

            TXTAdd1.Text = SelectedProjectAddressDetails.Add1;
            TXTAdd2.Text = SelectedProjectAddressDetails.Add2;
            TXTAdd3.Text = SelectedProjectAddressDetails.Add3;
            TXTCity.Text = SelectedProjectAddressDetails.City;
            TXTPostCode.Text = SelectedProjectAddressDetails.PostCode;
            if (SelectedProjectAddressDetails.bCreateAddressRecord)
            {
                int iUKIndexValue = CBOCountry.FindStringExact("United Kingdom");

                if (iUKIndexValue > -1)
                {
                    CBOCountry.SelectedIndex = iUKIndexValue;
                }
                else
                {
                    CBOCountry.SelectedIndex = 0;
                }
            }
            else
            {
                CBOCountry.SelectedValue = SelectedProjectAddressDetails.CountryCode;
                CBOState.SelectedValue = SelectedProjectAddressDetails.State;
            }
            ValidateAndEnable();
        }
        class PositionDetails
        {
            private bool bSelected;
            private bool bPrimary;
            private int iPosition;
            private string sxeUserName;
            private bool bFoundRecord;
            private string sProject;
            private string sOriginator;
            private int iIdentityDivisionID;
            private int iContactID;
            private string sCompCode;
            public int iPos
            {
                get
                {
                    return iPosition;
                }

                set
                {
                    iPosition = value;
                }
            }

            public bool bCreatePositionDetailsRecord
            {
                get
                {
                    return !(bFoundRecord);
                }
            }

            public bool bIsSelected
            {
                get
                {
                    return bSelected;
                }
            }

            public bool bIsPrimary
            {
                get
                {
                    return bPrimary;
                }
                set
                {
                    bPrimary = value;
                }
            }

            public string sPerson
            {
                get
                {
                    return sxeUserName;
                }

                set
                {
                    sxeUserName = value;
                }
            }
            public string sProjectCode
            {
                get
                {
                    return sProject;
                }
                set
                {
                    sProject = value;
                }
            }
            public string sOrig
            {
                get
                {
                    return sOriginator;
                }
                set
                {
                    sOriginator = value;
                }
            }
            public int iIdentity
            {
                get
                {
                    return iIdentityDivisionID;
                }
                set
                {
                    iIdentityDivisionID = value;
                }
            }
            public string sCompanyCode
            {
                get
                {
                    return sCompCode;
                }
                set
                {
                    sCompCode = value;
                }
            }
            public int iContact
            {
                get
                {
                    return iContactID;
                }
                set
                {
                    iContactID = value;
                }
            }
            public bool DelPersonPosition(string sConnectionString)
            {
                using (SqlConnection conn = new SqlConnection())
                {
                    conn.ConnectionString = sConnectionString;
                    conn.Open();

                    SqlCommand cmd = new SqlCommand("Utility.DelPrjCpyPosUsersInd", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter PositionParameter = new SqlParameter("@Pos_Id", SqlDbType.Int);
                    PositionParameter.Value = iPosition;
                    cmd.Parameters.Add(PositionParameter);

                    SqlParameter UsernameParameter = new SqlParameter("@xe_username", SqlDbType.Char);
                    UsernameParameter.Value = sxeUserName;
                    cmd.Parameters.Add(UsernameParameter);

                    SqlParameter ProjectParameter = new SqlParameter("@ProjectCode", SqlDbType.Char);
                    ProjectParameter.Value = sProject;
                    cmd.Parameters.Add(ProjectParameter);

                    SqlParameter OrigParameter = new SqlParameter("@Originator", SqlDbType.Char);
                    OrigParameter.Value = sOrig;
                    cmd.Parameters.Add(OrigParameter);

                    cmd.ExecuteNonQuery();

                    return true;
                }
            }
            public bool GetPersonPosition(string sConnectionString)
            {
                using (SqlConnection conn = new SqlConnection())
                {
                    conn.ConnectionString = sConnectionString;
                    conn.Open();

                    SqlCommand cmd = new SqlCommand("Utility.GetPrjCpyPosUsersInd", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter PositionParameter = new SqlParameter("@Pos_Id", SqlDbType.Int);
                    PositionParameter.Value = iPosition;
                    cmd.Parameters.Add(PositionParameter);

                    SqlParameter UsernameParameter = new SqlParameter("@xe_username", SqlDbType.Char);
                    UsernameParameter.Value = sxeUserName;
                    cmd.Parameters.Add(UsernameParameter);

                    SqlParameter ProjectParameter = new SqlParameter("@ProjectCode", SqlDbType.Char);
                    ProjectParameter.Value = sProject;
                    cmd.Parameters.Add(ProjectParameter);

                    SqlParameter OrigParameter = new SqlParameter("@Originator", SqlDbType.Char);
                    OrigParameter.Value = sOrig;
                    cmd.Parameters.Add(OrigParameter);

                    SqlDataReader rdr = cmd.ExecuteReader();
                    rdr.Read();

                    bFoundRecord = false;
                    if (rdr.HasRows)
                    {
                        bSelected = (bool)rdr[0];
                        bPrimary = (bool)rdr[1];
                        iIdentity = (int)rdr[2];
                        iContact = (int)rdr[3];
                        bFoundRecord = true;
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }

            public bool UpdatePersonPosition(string sConnectionString)
            {
                using (SqlConnection conn = new SqlConnection())
                {
                    conn.ConnectionString = sConnectionString;
                    conn.Open();

                    SqlCommand cmd = new SqlCommand("Utility.UpdPrjCpyPosUsersInd", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter ProjectParameter = new SqlParameter("@ProjectCode", SqlDbType.Char);
                    ProjectParameter.Value = sProjectCode;
                    cmd.Parameters.Add(ProjectParameter);

                    SqlParameter PositionParameter = new SqlParameter("@PosId", SqlDbType.Int);
                    PositionParameter.Value = iPosition;
                    cmd.Parameters.Add(PositionParameter);

                    SqlParameter UsernameParameter = new SqlParameter("@xe_username", SqlDbType.Char);
                    UsernameParameter.Value = sxeUserName;
                    cmd.Parameters.Add(UsernameParameter);

                    SqlParameter IdentityParameter = new SqlParameter("@iIdentity", SqlDbType.Int);
                    IdentityParameter.Value = iIdentity;
                    cmd.Parameters.Add(IdentityParameter);

                    SqlParameter ContactParameter = new SqlParameter("@iContact", SqlDbType.Int);
                    ContactParameter.Value = iContact;
                    cmd.Parameters.Add(ContactParameter);

                    cmd.ExecuteNonQuery();

                    return true;
                }
            }

            public bool AddPersonPosition(string sConnectionString)
            {
                using (SqlConnection conn = new SqlConnection())
                {
                    conn.ConnectionString = sConnectionString;
                    conn.Open();

                    SqlCommand cmd = new SqlCommand("Utility.InsPrjCpyPosUsersInd", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter ProjectParameter = new SqlParameter("@ie_code", SqlDbType.Char);
                    ProjectParameter.Value = sProjectCode;
                    cmd.Parameters.Add(ProjectParameter);

                    SqlParameter PositionParameter = new SqlParameter("@Pos_Id", SqlDbType.Int);
                    PositionParameter.Value = iPosition;
                    cmd.Parameters.Add(PositionParameter);

                    SqlParameter UsernameParameter = new SqlParameter("@xe_username", SqlDbType.Char);
                    UsernameParameter.Value = sxeUserName;
                    cmd.Parameters.Add(UsernameParameter);

                    SqlParameter PrimaryParameter = new SqlParameter("@is_primary", SqlDbType.Bit);
                    PrimaryParameter.Value = bIsPrimary;
                    cmd.Parameters.Add(PrimaryParameter);

                    SqlParameter OrigParameter = new SqlParameter("@Originator", SqlDbType.Char);
                    OrigParameter.Value = sOrig;
                    cmd.Parameters.Add(OrigParameter);

                    SqlParameter IdentityParameter = new SqlParameter("@Entity_Division_id", SqlDbType.Char);
                    IdentityParameter.Value = iIdentity;
                    cmd.Parameters.Add(IdentityParameter);

                    SqlParameter CompanyParameter = new SqlParameter("@CompanyCode", SqlDbType.Char);
                    CompanyParameter.Value = sCompCode;
                    cmd.Parameters.Add(CompanyParameter);

                    SqlParameter ContactParameter = new SqlParameter("@contact_id", SqlDbType.Int);
                    ContactParameter.Value = iContact;
                    cmd.Parameters.Add(ContactParameter);

                    SqlParameter BranchParameter = new SqlParameter("@Branch_Code", SqlDbType.Char);
                    BranchParameter.Value = "";
                    cmd.Parameters.Add(BranchParameter);

                    cmd.ExecuteNonQuery();

                    return true;
                }
            }
        }
        class ProjectDetails
        {
            private string sProjectName;
            private bool bFoundRecord;
            private bool bPrinciple;

            public bool bCreateProjectRecord
            {
                get
                {
                    return !(bFoundRecord);
                }
            }

            public string ProjectName
            {
                get
                {
                    return sProjectName;
                }
                set
                {
                    sProjectName = value;
                }
            }
            public bool PrincipleProject
            {
                get
                {
                    return bPrinciple;
                }
                set
                {
                    bPrinciple = value;
                }
            }

            public bool GetProject(string ipsProjectCode, string sConnectionString, string sOrig)
            {
                using (SqlConnection conn = new SqlConnection())
                {
                    conn.ConnectionString = sConnectionString;
                    conn.Open();

                    SqlCommand cmd = new SqlCommand("Utility.GetPrjCpyProjectDetails", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter ProjectParameter = new SqlParameter("@ProjectCode", SqlDbType.Char);
                    ProjectParameter.Value = ipsProjectCode;
                    cmd.Parameters.Add(ProjectParameter);

                    SqlParameter OrigParameter = new SqlParameter("@Originator", SqlDbType.Char);
                    OrigParameter.Value = sOrig;
                    cmd.Parameters.Add(OrigParameter);

                    SqlDataReader rdr = cmd.ExecuteReader();
                    rdr.Read();

                    bFoundRecord = false;
                    if (rdr.HasRows)
                    {
                        sProjectName = Convert.ToString(rdr[0]);
                        bPrinciple = (bool)rdr[1];
                        bFoundRecord = true;
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
            }
            public int UpdateProject(string ipsProjectCode, string sConnectionString, string sOrig)
            {
                using (SqlConnection conn = new SqlConnection())
                {
                    conn.ConnectionString = sConnectionString;
                    conn.Open();

                    SqlCommand cmd = new SqlCommand("Utility.UpdPrjCpyProjectDetails", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter ProjectParameter = new SqlParameter("@ProjectCode", SqlDbType.Char);
                    ProjectParameter.Value = ipsProjectCode;
                    cmd.Parameters.Add(ProjectParameter);

                    SqlParameter ProjectNameParameter = new SqlParameter("@ProjectName", SqlDbType.Char);
                    ProjectNameParameter.Value = this.sProjectName;
                    cmd.Parameters.Add(ProjectNameParameter);

                    SqlParameter OrigParameter = new SqlParameter("@Originator", SqlDbType.Char);
                    OrigParameter.Value = sOrig;
                    cmd.Parameters.Add(OrigParameter);

                    SqlParameter PrincipleProjectParameter = new SqlParameter("@PrincipleProject", SqlDbType.Char);
                    PrincipleProjectParameter.Value = bPrinciple;
                    cmd.Parameters.Add(PrincipleProjectParameter);

                    cmd.ExecuteNonQuery();

                    return 1;
                }
            }
            public int AddNewProject(string ipsProjectCode, string sConnectionString, string sOrig)
            {
                using (SqlConnection conn = new SqlConnection())
                {
                    conn.ConnectionString = sConnectionString;
                    conn.Open();

                    SqlCommand cmd = new SqlCommand("Utility.InsPrjCpyProjectDetails", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter ProjectParameter = new SqlParameter("@ProjectCode", SqlDbType.Char);
                    ProjectParameter.Value = ipsProjectCode;
                    cmd.Parameters.Add(ProjectParameter);

                    SqlParameter OrigParameter = new SqlParameter("@Originator", SqlDbType.Char);
                    OrigParameter.Value = sOrig;
                    cmd.Parameters.Add(OrigParameter);

                    SqlParameter ProjectNameParameter = new SqlParameter("@ProjectName", SqlDbType.Char);
                    ProjectNameParameter.Value = sProjectName;
                    cmd.Parameters.Add(ProjectNameParameter);

                    SqlParameter PrincipleProjectParameter = new SqlParameter("@PrincipleProject", SqlDbType.Char);
                    PrincipleProjectParameter.Value = bPrinciple;
                    cmd.Parameters.Add(PrincipleProjectParameter);

                    cmd.ExecuteNonQuery();

                    return 1;
                }
            }
        }

        class ProjectAddressDetails
        {
            string sProjectCode;
            string sAdd1;
            string sAdd2;
            string sAdd3;
            string sCity;
            string sPostCode;
            string sCountryCode;
            string sState;
            string sOriginator;
            bool bFoundRecord;
            public string sProjCode
            {
                get
                {
                    return sProjectCode;
                }
                set
                {
                    sProjectCode = value;
                }
            }

            public bool bCreateAddressRecord
            {
                get
                {
                    return !(bFoundRecord);
                }
            }

            public string Add1
            {
                get
                {
                    return sAdd1;
                }
                set
                {
                    sAdd1 = value;
                }
            }
            public string Add2
            {
                get
                {
                    return sAdd2;
                }
                set
                {
                    sAdd2 = value;
                }
            }
            public string Add3
            {
                get
                {
                    return sAdd3;
                }
                set
                {
                    sAdd3 = value;
                }
            }
            public string City
            {
                get
                {
                    return sCity;
                }
                set
                {
                    sCity = value;
                }
            }
            public string PostCode
            {
                get
                {
                    return sPostCode;
                }
                set
                {
                    sPostCode = value;
                }
            }
            public string State
            {
                get
                {
                    return sState;
                }
                set
                {
                    sState = value;
                }
            }
            public string CountryCode
            {
                get
                {
                    return sCountryCode;
                }
                set
                {
                    sCountryCode = value;
                }
            }
            public string sOrig
            {
                get
                {
                    return sOriginator;
                }
                set
                {
                    sOriginator = value;
                }
            }
            public bool GetAddress(string sConnectionString)
            {
                using (SqlConnection conn = new SqlConnection())
                {
                    conn.ConnectionString = sConnectionString;
                    conn.Open();

                    SqlCommand cmd = new SqlCommand("Utility.GetPrjCpyProjectAddress", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter ProjectParameter = new SqlParameter("@ProjectCode", SqlDbType.Char);
                    ProjectParameter.Value = sProjectCode;
                    cmd.Parameters.Add(ProjectParameter);

                    SqlParameter OrigParameter = new SqlParameter("@Originator", SqlDbType.Char);
                    OrigParameter.Value = sOrig;
                    cmd.Parameters.Add(OrigParameter);

                    SqlDataReader rdr = cmd.ExecuteReader();
                    rdr.Read();

                    bFoundRecord = false;
                    if (rdr.HasRows)
                    {
                        sAdd1 = (string)rdr[0];
                        sAdd2 = (string)rdr[1];
                        sAdd3 = (string)rdr[2];
                        sCity = (string)rdr[3];
                        sPostCode = (string)rdr[4];
                        sCountryCode = (string)rdr[5];
                        sState = (string)rdr[6];
                        bFoundRecord = true;
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
            }
            public bool AddNewAddress(string sConnectionString)
            {
                using (SqlConnection conn = new SqlConnection())
                {
                    conn.ConnectionString = sConnectionString;
                    conn.Open();

                    SqlCommand cmd = new SqlCommand("Utility.InsPrjCpyProjectAddress", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter ProjectParameter = new SqlParameter("@ProjectCode", SqlDbType.Char);
                    ProjectParameter.Value = sProjectCode;
                    cmd.Parameters.Add(ProjectParameter);

                    SqlParameter Add1Parameter = new SqlParameter("@Add1", SqlDbType.Char);
                    Add1Parameter.Value = sAdd1;
                    cmd.Parameters.Add(Add1Parameter);

                    SqlParameter Add2Parameter = new SqlParameter("@Add2", SqlDbType.Char);
                    Add2Parameter.Value = sAdd2;
                    cmd.Parameters.Add(Add2Parameter);

                    SqlParameter Add3Parameter = new SqlParameter("@Add3", SqlDbType.Char);
                    Add3Parameter.Value = sAdd3;
                    cmd.Parameters.Add(Add3Parameter);

                    SqlParameter CityParameter = new SqlParameter("@City", SqlDbType.Char);
                    CityParameter.Value = sCity;
                    cmd.Parameters.Add(CityParameter);

                    SqlParameter PostCodeParameter = new SqlParameter("@Postcode", SqlDbType.Char);
                    PostCodeParameter.Value = sPostCode;
                    cmd.Parameters.Add(PostCodeParameter);

                    SqlParameter CountryCodeParameter = new SqlParameter("@CountryCode", SqlDbType.Char);
                    CountryCodeParameter.Value = sCountryCode;
                    cmd.Parameters.Add(CountryCodeParameter);

                    SqlParameter StateCodeParameter = new SqlParameter("@StateCode", SqlDbType.Char);
                    StateCodeParameter.Value = sState;
                    cmd.Parameters.Add(StateCodeParameter);

                    SqlParameter OriginatorParameter = new SqlParameter("@Originator", SqlDbType.Char);
                    OriginatorParameter.Value = sOriginator;
                    cmd.Parameters.Add(OriginatorParameter);

                    cmd.ExecuteNonQuery();

                    bFoundRecord = true;

                    return true;
                }
            }
            public int UpdateAddress(string sConnectionString)
            {
                using (SqlConnection conn = new SqlConnection())
                {
                    conn.ConnectionString = sConnectionString;
                    conn.Open();

                    SqlCommand cmd = new SqlCommand("Utility.UpdPrjCpyProjectAddress", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter ProjectParameter = new SqlParameter("@ProjectCode", SqlDbType.Char);
                    ProjectParameter.Value = sProjectCode;
                    cmd.Parameters.Add(ProjectParameter);

                    SqlParameter Add1Parameter = new SqlParameter("@Add1", SqlDbType.Char);
                    Add1Parameter.Value = sAdd1;
                    cmd.Parameters.Add(Add1Parameter);

                    SqlParameter Add2Parameter = new SqlParameter("@Add2", SqlDbType.Char);
                    Add2Parameter.Value = sAdd2;
                    cmd.Parameters.Add(Add2Parameter);

                    SqlParameter Add3Parameter = new SqlParameter("@Add3", SqlDbType.Char);
                    Add3Parameter.Value = sAdd3;
                    cmd.Parameters.Add(Add3Parameter);

                    SqlParameter CityParameter = new SqlParameter("@City", SqlDbType.Char);
                    CityParameter.Value = sCity;
                    cmd.Parameters.Add(CityParameter);

                    SqlParameter PostCodeParameter = new SqlParameter("@Postcode", SqlDbType.Char);
                    PostCodeParameter.Value = sPostCode;
                    cmd.Parameters.Add(PostCodeParameter);

                    SqlParameter CountryCodeParameter = new SqlParameter("@CountryCode", SqlDbType.Char);
                    CountryCodeParameter.Value = sCountryCode;
                    cmd.Parameters.Add(CountryCodeParameter);

                    SqlParameter StateCodeParameter = new SqlParameter("@StateCode", SqlDbType.Char);
                    StateCodeParameter.Value = sState;
                    cmd.Parameters.Add(StateCodeParameter);

                    SqlParameter OriginatorParameter = new SqlParameter("@Originator", SqlDbType.Char);
                    OriginatorParameter.Value = sOriginator;
                    cmd.Parameters.Add(OriginatorParameter);

                    cmd.ExecuteNonQuery();

                    bFoundRecord = true;

                    return 1;
                }
            }
        }
        private void Form2_Load(object sender, EventArgs e)
        {
        //Initialise state and country code Combo's.

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = sConnectionString;
                conn.Open();

                SqlCommand cmd = new SqlCommand("Utility.GetCountry", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter da = new SqlDataAdapter(cmd);

                DataTable dt = new DataTable();

                da.Fill(dt);

                BindingSource bs1 = new BindingSource();
                bs1.DataSource = dt;
                CBOCountry.DataSource = bs1;

                CBOCountry.DisplayMember = "Country_name";
                CBOCountry.ValueMember = "Country_Code";

                int iUKIndexValue = CBOCountry.Items.IndexOf("United Kingdom");

                if (iUKIndexValue > -1)
                {
                    CBOCountry.SelectedIndex = iUKIndexValue;
                }
                else
                {
                CBOCountry.SelectedIndex = 0;
                }
            }

            //Set the initial position to the first entry in the position list and retrieve the details.
            //Populate the positions List Box.

                using (SqlConnection conn = new SqlConnection())
                {
                    conn.ConnectionString = sConnectionString;
                    conn.Open();

                    SqlCommand cmd = new SqlCommand("Utility.GetProjectOrganisation", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataAdapter da = new SqlDataAdapter(cmd);

                    DataTable dt = new DataTable();

                    da.Fill(dt);

                    BindingSource bs1 = new BindingSource();
                    bs1.DataSource = dt;
                    LSTPositions.DataSource = bs1;

                    LSTPositions.DisplayMember = "POS_ROLE";
                    LSTPositions.ValueMember = "POS_ID";
                }

                InitialiseDataGridColumns();

                PopulateProjectList();

            //  Set the initial project code set to the first entry in the project list and retrieve the details.
                sSelectedProjectCode = LSTProject.SelectedValue.ToString();

                PopulateProjectDetails();

                sSelectedPosition = LSTPositions.SelectedValue.ToString();
                iSelectedPosition = Convert.ToInt32(sSelectedPosition);

//Get details of those that are allowed to be involved.
                GetPersonsAllowedToBeInvolved();

//Get existing details for the project/position

                GetSelectedPosInd();

                isInitialised = true;
            }

            private void LSTPositions_SelectedIndexChanged(object sender, EventArgs e)
            {
                DGVPersonsInvolved.AutoGenerateColumns = false;
                if (isInitialised)
                {
                    UpdatePosIndDetails();

                    sSelectedPosition = LSTPositions.SelectedValue.ToString();
                    iSelectedPosition = Convert.ToInt32(sSelectedPosition);

                    GetPersonsAllowedToBeInvolved();
                    GetSelectedPosInd();
                }
            }
            private void DGVPersonsInvolved_CellContentClick(object sender, DataGridViewCellEventArgs e)
            {
                if (e.ColumnIndex == 1 && e.RowIndex >= 0)
                    this.DGVPersonsInvolved.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
            private void DGVPersonsInvolved_CellValueChanged(object sender, DataGridViewCellEventArgs e)
            {
                if (isInitialised)
                {
                }
            }

            private void LSTProject_SelectedIndexChanged(object sender, EventArgs e)
            {
                if (isInitialised)
                {
                    GRPProjectDetails.Text = string.Concat("Project Selected: ", LSTProject.SelectedValue);
                    // Create or update the previously selected project details.
                    UpdateProjectDetails();

                    // Now get the newly selected project details and populate the form fields.
                    PopulateProjectDetails();

                    UpdatePosIndDetails();
                    sSelectedProjectCode = LSTProject.SelectedValue.ToString();
                    GetPersonsAllowedToBeInvolved();
                    GetSelectedPosInd();
                }
            }

        private void BTNCancel_Click(object sender, EventArgs e)
        {
            string message = "All changes will be lost - Are you sure you wish to cancel?";
            string caption = "Cancel";
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult result;

            result = MessageBox.Show(message, caption, buttons);

            if (result == DialogResult.Yes)
            {
                sConnectionString = Properties.Settings.Default.ExternalDataConnectionString;
                sOriginator = Environment.UserDomainName + "\\" + Environment.UserName;

                using (SqlConnection conn = new SqlConnection())
                {
                    conn.ConnectionString = sConnectionString;
                    conn.Open();

                    SqlCommand cmd = new SqlCommand("Utility.PrjCpyCleanup", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter OriginatorParameter = new SqlParameter("@Originator", SqlDbType.Char);
                    OriginatorParameter.Value = sOriginator;
                    cmd.Parameters.Add(OriginatorParameter);

                    cmd.ExecuteNonQuery();
                    conn.Close();

                    this.Close();
                }
            }
        }

        private void BTN_CreateProjects_Click(object sender, EventArgs e)
        {
            UpdateProjectDetails();

            UpdatePosIndDetails();

            int iResult;

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = sConnectionString;
                conn.Open();

                SqlCommand cmd = new SqlCommand("Utility.ValidatePositionsData", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter OrigParameter = new SqlParameter("@Originator", SqlDbType.VarChar);
                OrigParameter.Value = sOriginator;
                cmd.Parameters.Add(OrigParameter);

                SqlParameter RetValParameter = new SqlParameter("@return_value", SqlDbType.Int);
                RetValParameter.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(RetValParameter);

                SqlParameter ErrorDetParameter = new SqlParameter("@ErrorDetails", SqlDbType.Char);
                ErrorDetParameter.Direction = ParameterDirection.Output;
                ErrorDetParameter.Size = 500;
                cmd.Parameters.Add(ErrorDetParameter);

                cmd.ExecuteScalar();

                iResult = (int)cmd.Parameters["@return_value"].Value;
                
                conn.Close();
                if (iResult == 1)
                {
                    MessageBox.Show(string.Concat("One or more of the positions has more than more primary contact, please check.", ErrorDetParameter.Value.ToString()),"Positions Validation",MessageBoxButtons.OK,MessageBoxIcon.Error);
                    return;
                }
                if (iResult == 2)
                {
                    MessageBox.Show(string.Concat("One or more of the positions does not have a primary, please check.", ErrorDetParameter.Value.ToString()),"Positions Validation", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                if (iResult == 3)
                {
                    MessageBox.Show(string.Concat("There must be a Primary Director, Operations Manager, and Commercial Manager set for all projects. The following do not meet this criteria: ", ErrorDetParameter.Value.ToString()),"Positions Validation",MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = sConnectionString;
                conn.Open();

                SqlCommand cmd = new SqlCommand("Utility.CheckPrincipleProject", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter OrigParameter = new SqlParameter("@Originator", SqlDbType.VarChar);
                OrigParameter.Value = sOriginator;
                cmd.Parameters.Add(OrigParameter);

                SqlParameter FCRequiredParam = new SqlParameter("@FrameWorkContractRequired", SqlDbType.Char);
                if (sTargetFrameworkContract == "")
                {
                    FCRequiredParam.Value = "N";
                }
                else
                {
                    FCRequiredParam.Value = "Y";
                }
                cmd.Parameters.Add(FCRequiredParam);

                SqlParameter RetValParameter = new SqlParameter("@return_value", SqlDbType.Int);
                RetValParameter.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(RetValParameter);

                cmd.ExecuteScalar();

                iResult = (int)cmd.Parameters["@return_value"].Value;

                conn.Close();
            }

            if (iResult == 1)
            {
                MessageBox.Show("There can only be one principle project. Please correct this and try again.","Principle Project Validation", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (iResult == 2)
            {
                MessageBox.Show("There must be one principle project when creating a new framework contract. Please correct this and try again.");
                return;
            }

            string message = "Are you sure you wish to create these projects?";
            string caption = "Project creation";
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult result;

            result = MessageBox.Show(message, caption, buttons);

            if (result == DialogResult.Yes)
            {
                CreateProjects();
            }
        }

        private void CBOCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            RefreshState();
        }

        private void CBOState_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        private void Form2_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            string message = "Are you sure you wish to exit this application?";
            string caption = "Exit Application";
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult result;

            result = MessageBox.Show(message, caption, buttons);

            if (result == DialogResult.Yes)
            {
                sConnectionString = Properties.Settings.Default.ExternalDataConnectionString;
                sOriginator = Environment.UserDomainName + "\\" + Environment.UserName;

                using (SqlConnection conn = new SqlConnection())
                {
                    conn.ConnectionString = sConnectionString;
                    conn.Open();

                    SqlCommand cmd = new SqlCommand("Utility.PrjCpyCleanup", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter OriginatorParameter = new SqlParameter("@Originator", SqlDbType.Char);
                    OriginatorParameter.Value = sOriginator;
                    cmd.Parameters.Add(OriginatorParameter);

                    cmd.ExecuteNonQuery();
                    conn.Close();

                    this.Close();
                }
            }
        }

        private void TXTProjectName_Validating(object sender, CancelEventArgs e)
        {
            if (TXTProjectName.Text==string.Empty)
            {
                ProjectNameValidator.SetError(TXTProjectName, "Please enter a name for the Project.");
                e.Cancel = true;
                TXTProjectName.Focus();
                bNameEntered = false;
            }
            else
            {
                ProjectNameValidator.SetError(TXTProjectName, null);
                bNameEntered = true;
                e.Cancel = false;
            }
            ValidateAndEnable();
        }

        private void TXT_Add1Validating(object sender, CancelEventArgs e)
        {
            if (TXTAdd1.Text == string.Empty)
            {
                AddressLine1Validator.SetError(TXTAdd1,"Please enter an address for the first address line.");
                e.Cancel = true;
                bAddressLine1Entered = false;
            }
            else
            {
                e.Cancel = false;
                AddressLine1Validator.SetError(TXTAdd1, null);
                bAddressLine1Entered = true;
            }
            ValidateAndEnable();
        }

        private void TXTPostCode_Validating(object sender, CancelEventArgs e)
        {
            if (TXTPostCode.Text == string.Empty)
            {
                PostCodeValidator.SetError(TXTPostCode, "Please enter a post code.");
                e.Cancel = true;
                bPostCodeEntered = false;
            }
            else
            {
                e.Cancel = false;
                PostCodeValidator.SetError(TXTPostCode, null);
                bPostCodeEntered = true;
            }
            ValidateAndEnable();
        }

        private void TXTProjectName_TextChanged(object sender, EventArgs e)
        {

        }

        private void CBOCountry_Validating(object sender, CancelEventArgs e)
        {
            if (CBOCountry.SelectedIndex == -1)
            {
                CountryValidator.SetError(CBOCountry, "Please select a country");
                bCountryEntered = false;
                e.Cancel = true;
            }
            else
            {
                CountryValidator.SetError(CBOCountry, "");
                bCountryEntered = true;
                e.Cancel = false;
            }
            ValidateAndEnable();
        }

        private void CBOState_Validating(object sender, CancelEventArgs e)
        {
            if (CBOCountry.SelectedValue.ToString() == "UK")
            {
                if (CBOState.SelectedIndex == -1)
                {
                    StateValidator.SetError(CBOState, "Please select a State");
                    bStateEntered = false;
                    e.Cancel = true;
                }
                else
                {
                    StateValidator.SetError(CBOState, "");
                    bStateEntered = true;
                    e.Cancel = false;
                }
            }
            ValidateAndEnable();
        }
    }
}




