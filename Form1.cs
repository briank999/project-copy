﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Windows.Controls;
using System.Configuration;

namespace Project_Copy
{
    public partial class FrmCopyProjectProposal : Form
    {
        public int iEnteredNoOfProjectProposals;
        public string sSourceFC;
        public string sTargetFC;
        public string sSourceProjectCode;
        public string sTargetProjectCode;
        public string sFCTitle;
        bool isInitialised = false;
        public int iNumberOfProjectsProposals;
        public int iNumberOfJNBProjects;
        public int iNumberOfMMProjects;
        public int iNumberOfMMJNBProjects;
        public int iNumberOfJBAProjects;
        public int iNumberOfJBAJNBProjects;
        public string sConnectionString;
        public string sOriginator;
        public string sProjCompanyCode;
        public string sFCCompanyCode;
        public string sProjectType;
        public FrmCopyProjectProposal()
        {
            InitializeComponent();
            sConnectionString = Properties.Settings.Default.ExternalDataConnectionString;
            sOriginator = Environment.UserDomainName + "\\" + Environment.UserName;

            sConnectionString = Properties.Settings.Default.ExternalDataConnectionString;
            sOriginator = Environment.UserDomainName + "\\" + Environment.UserName;

            using (SqlConnection conn = new SqlConnection())
            {
                try
                {
                    conn.ConnectionString = sConnectionString;
                    conn.Open();

                    SqlCommand cmd = new SqlCommand("Utility.PrjCpyCleanup", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter OriginatorParameter = new SqlParameter("@Originator", SqlDbType.Char);
                    OriginatorParameter.Value = sOriginator;
                    cmd.Parameters.Add(OriginatorParameter);

                    try
                    {
                        cmd.ExecuteNonQuery();
                    }
                    catch (SqlException ex)
                    {
                        string message = "There was an error while removing project details. Please contact IT. The error is shown below:\n\n";
                        message = string.Concat(message, ex.Message.ToString());

                        string caption = "Project Creation";
                        MessageBoxButtons buttons = MessageBoxButtons.OK;
                        DialogResult result;

                        result = MessageBox.Show(message, caption, buttons);
                        Environment.Exit(1);
                    }
                }
                catch (SqlException ex)
                {
                    string message = "There was an error while connecting to the database. Please contact IT. The error is shown below:\n\n";
                    message = string.Concat(message, ex.Message.ToString());

                    string caption = "Project Creation";
                    MessageBoxButtons buttons = MessageBoxButtons.OK;
                    DialogResult result;

                    result = MessageBox.Show(message, caption, buttons);
                    Environment.Exit(1);
                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = sConnectionString;
                conn.Open();

                SqlCommand cmd = new SqlCommand("Utility.GetProjectCompanyList", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter da = new SqlDataAdapter(cmd);

                DataTable dt = new DataTable();

                da.Fill(dt);

                BindingSource bs1 = new BindingSource();
                bs1.DataSource = dt;

                CBOProjCompany.DataSource = bs1;

                CBOProjCompany.DisplayMember = "COMPANY_NAME";
                CBOProjCompany.ValueMember = "COMPANY_CODE";

                sProjCompanyCode = CBOProjCompany.SelectedValue.ToString();
                CBOProjCompany_SelectedIndexChanged(this, new EventArgs());
            }

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = sConnectionString;
                conn.Open();

                SqlCommand cmd = new SqlCommand("Utility.GetFCCompanyList", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter da = new SqlDataAdapter(cmd);

                DataTable dt = new DataTable();

                da.Fill(dt);

                BindingSource bs1 = new BindingSource();
                bs1.DataSource = dt;

                CBOFCCompany.DataSource = bs1;

                CBOFCCompany.DisplayMember = "COMPANY_NAME";
                CBOFCCompany.ValueMember = "COMPANY_CODE";

                isInitialised = true;
                sFCCompanyCode = CBOFCCompany.SelectedValue.ToString();
                CBOFCCompany_SelectedIndexChanged(this, new EventArgs());
            }

            string sTTText;

            sTTText = "The code you enter will be given a 2 digit numerical value following the code you have defined\n and start from the next available number after the highest number \n(i.e. NC01, NC02, NC05, NCO7 exist and you create 2 projects after entering\n a prefix of “NC”, the projects will be created with the codes NC08 and NC09”). \nThe available prefixes you can enter are (which will have 2 number code added when created);-\n\ni)  AA\nii)	AANN\niii)	AANN -";
            TTCodeFormat.SetToolTip(this.TXTNewFrameWorkContract, sTTText);
            TTCodeFormat.SetToolTip(this.TXTNewProjectCode, sTTText);
            TTCodeFormat.AutoPopDelay = 5000;
            TTCodeFormat.InitialDelay = 10;
            TTCodeFormat.ReshowDelay = 100;

            RBCopyFrameWorkContract.Checked = false;
            RBCopyProject.Checked = true;

            CBOFCCompany.SelectedValue = "JBAB";
            CBOFCCompany_SelectedIndexChanged(this, new EventArgs());

            isInitialised = true;
        }
        private void CBOFrameworkContract_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void CBOProjCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (isInitialised)
            {
                using (SqlConnection conn = new SqlConnection())
                {
                    conn.ConnectionString = sConnectionString;
                    conn.Open();

                    SqlCommand cmd = new SqlCommand("Utility.GetProjectProposal", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter CompanyParameter = new SqlParameter("@CompanyCode", SqlDbType.Char);
                    CompanyParameter.Value = CBOProjCompany.SelectedValue.ToString();

                    cmd.Parameters.Add(CompanyParameter);

                    SqlParameter ProjectOrProposalParameter = new SqlParameter("@ProjectOrProposal", SqlDbType.Char);

                    if (RBProject.Checked)
                    {
                        ProjectOrProposalParameter.Value = "P";
                    }

                    if (RBProposal.Checked)
                    {
                        ProjectOrProposalParameter.Value = 'O';
                    }

                    if (!RBProposal.Checked || !RBProject.Checked)
                    {
                        ProjectOrProposalParameter.Value = 'P';
                    }

                    cmd.Parameters.Add(ProjectOrProposalParameter);

                    SqlDataAdapter da = new SqlDataAdapter(cmd);

                    DataTable dt = new DataTable();

                    da.Fill(dt);

                    BindingSource bs1 = new BindingSource();
                    bs1.DataSource = dt;

                    CBOProjectProposal.DataSource = bs1;

                    CBOProjectProposal.DisplayMember = "IE_NAME";
                    CBOProjectProposal.ValueMember = "IE_CODE";
                }
            }
        }
        private void CBOFCCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (isInitialised)
            {
                using (SqlConnection conn = new SqlConnection())
                {
                    conn.ConnectionString = sConnectionString;
                    conn.Open();

                    SqlCommand cmd = new SqlCommand("Utility.GetFrameworkContract", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter CompanyParameter = new SqlParameter("@CompanyCode", SqlDbType.Char);
                    CompanyParameter.Value = CBOFCCompany.SelectedValue.ToString();

                    cmd.Parameters.Add(CompanyParameter);

                    SqlDataAdapter da = new SqlDataAdapter(cmd);

                    DataTable dt = new DataTable();

                    da.Fill(dt);

                    BindingSource bs1 = new BindingSource();
                    bs1.DataSource = dt;

                    CBOFrameworkContract.DataSource = bs1;

                    CBOFrameworkContract.DisplayMember = "FRAMEWORK_CONTRACT_NAME";
                    CBOFrameworkContract.ValueMember = "FRAMEWORK_CONTRACT_CODE";
                }

                if (CBOFCCompany.SelectedValue.ToString() == "JBAB")
                {
                    TXTNoOfJBAProjects.Enabled = true;
                }

                if (CBOFCCompany.SelectedValue.ToString() != "JBAB")
                {
                    TXTNoOfJBAProjects.Enabled = false;
                    TXTNoOfJBAProjects.Text = "";
                }

                if (CBOFCCompany.SelectedValue.ToString() != "MMB")
                {
                    TXTNoMMProjects.Enabled = false;
                    TXTNoMMProjects.Text = "";
                }

                if (CBOFCCompany.SelectedValue.ToString() == "MMB")
                {
                    TXTNoMMProjects.Enabled = true;
                }
            }
        }
        private void RBProject_CheckedChanged(object sender, EventArgs e)
        {
            if (isInitialised)
            {
                using (SqlConnection conn = new SqlConnection())
                {
                    conn.ConnectionString = sConnectionString;
                    conn.Open();

                    SqlCommand cmd = new SqlCommand("Utility.GetProjectProposal", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter ProjectOrProposalParameter = new SqlParameter("@ProjectOrProposal", SqlDbType.Char);

                    if (RBProject.Checked)
                    {
                        ProjectOrProposalParameter.Value = "P";
                    }

                    if (RBProposal.Checked)
                    {
                        ProjectOrProposalParameter.Value = 'O';
                    }

                    cmd.Parameters.Add(ProjectOrProposalParameter);

                    SqlParameter CompanyParameter = new SqlParameter("@CompanyCode", SqlDbType.Char);
                    CompanyParameter.Value = CBOProjCompany.SelectedValue.ToString();

                    cmd.Parameters.Add(CompanyParameter);

                    SqlDataAdapter da = new SqlDataAdapter(cmd);

                    DataTable dt = new DataTable();

                    da.Fill(dt);

                    BindingSource bs1 = new BindingSource();
                    bs1.DataSource = dt;

                    CBOProjectProposal.DataSource = bs1;

                    CBOProjectProposal.DisplayMember = "IE_NAME";
                    CBOProjectProposal.ValueMember = "IE_CODE";

                    conn.Close();
                }
            }
        }

        private void RBProposal_CheckedChanged(object sender, EventArgs e)
        {
            if (isInitialised)
            {
                using (SqlConnection conn = new SqlConnection())
                {
                    conn.ConnectionString = sConnectionString;
                    conn.Open();

                    SqlCommand cmd = new SqlCommand("Utility.GetProjectProposal", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter CompanyParameter = new SqlParameter("@CompanyCode", SqlDbType.Char);
                    CompanyParameter.Value = CBOProjCompany.SelectedValue.ToString();

                    cmd.Parameters.Add(CompanyParameter);

                    SqlParameter ProjectOrProposalParameter = new SqlParameter("@ProjectOrProposal", SqlDbType.Char);

                    if (RBProject.Checked)
                    {
                        ProjectOrProposalParameter.Value = "P";
                    }

                    if (RBProposal.Checked)
                    {
                        ProjectOrProposalParameter.Value = 'O';
                    }

                    cmd.Parameters.Add(ProjectOrProposalParameter);

                    SqlDataAdapter da = new SqlDataAdapter(cmd);

                    DataTable dt = new DataTable();

                    da.Fill(dt);

                    BindingSource bs1 = new BindingSource();
                    bs1.DataSource = dt;

                    CBOProjectProposal.DataSource = bs1;

                    CBOProjectProposal.DisplayMember = "IE_NAME";
                    CBOProjectProposal.ValueMember = "IE_CODE";
                }

            }
        }

        private void CBOProjectProposal_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void RBCopyFrameWorkContract_CheckedChanged(object sender, EventArgs e)
        {
            if (RBCopyFrameWorkContract.Checked)
            {
                CBOFrameworkContract.Enabled = true;
                //RBProject.Enabled = false;
                //RBProposal.Enabled = false;
                //CBOProjectProposal.Enabled = false;
                CBOProjCompany.SelectedValue = "JNB";
                CBOProjCompany.Enabled = false;
                TXTNewProjectCode.Enabled = false;
            }
            if (!RBCopyFrameWorkContract.Checked)
            {
                CBOFrameworkContract.Enabled = false;
                CBOProjCompany.SelectedValue = "JNB";
                CBOProjCompany.Enabled = false;
                TXTNewProjectCode.Enabled = true;
                //RBProject.Enabled = true;
                //RBProposal.Enabled = true;
                //CBOProjectProposal.Enabled = true;
            }
        }

        private void RBCopyProject_CheckedChanged(object sender, EventArgs e)
        {
            if (RBCopyProject.Checked)
            {
                CBOFrameworkContract.Enabled = false;
                CBOFCCompany.Enabled = false;
                TXTNewFrameWorkContract.Enabled = false;
                TXTFCTitle.Enabled = false;
                CBOProjCompany.Enabled = false;
                CBOProjCompany.SelectedValue = "JNB";
                TXTNoJNBProjects.Enabled = true;
                TXTNoMMProjects.Enabled = true;
                TXTNoOfJBAProjects.Enabled = true;
            }

            if (!RBCopyProject.Checked)
            {
                CBOFrameworkContract.Enabled = true;
                CBOFCCompany.Enabled = true;
                CBOProjCompany.Enabled = true;
                TXTNewFrameWorkContract.Enabled = true;
                TXTFCTitle.Enabled = true;
            }
        }

        private void BTNRoles_Click(object sender, EventArgs e)
        {
            int iTestValue;
            int iResult;
            if (RBCopyProject.Checked && CBOProjectProposal.SelectedIndex == -1)
            {
                MessageBox.Show("Please select a project to copy from");
                return;
            }

            if (RBCopyFrameWorkContract.Checked && CBOFrameworkContract.SelectedIndex == -1)
            {
                MessageBox.Show("Please select a framework contract to copy from");
                return;
            }

            if (RBCopyProject.Checked && TXTNewProjectCode.Text == "")
            {
                MessageBox.Show("Please provide a project code to copy to");
                return;
            }

            if (RBCopyFrameWorkContract.Checked && TXTNewFrameWorkContract.Text == "")
            {
                MessageBox.Show("Please provide a FrameWork Contract code to copy to");
                return;
            }

            if (CBOFCCompany.SelectedValue.ToString() == "JNB")
            {
                if (!int.TryParse(TXTNoJNBProjects.Text, out iTestValue) || TXTNoJNBProjects.Text == "")
                {
                    MessageBox.Show("Please enter the number of JNB project(s)/Proposal(s) to create");
                    return;
                }
            }

            if (CBOFCCompany.SelectedValue.ToString() == "MMB")
            {
                if (!int.TryParse(TXTNoMMProjects.Text, out iTestValue) || TXTNoMMProjects.Text == "")
                {
                    MessageBox.Show("Please enter the number of MM project(s)/Proposal(s) to create");
                    return;
                }
            }

            if (CBOFCCompany.SelectedValue.ToString() == "JBAB")
            {
                if (!int.TryParse(TXTNoOfJBAProjects.Text, out iTestValue) || TXTNoOfJBAProjects.Text == "")
                {
                    MessageBox.Show("Please enter the number of JBA project(s)/Proposal(s) to create");
                    return;
                }
            }

            sProjCompanyCode = CBOProjCompany.SelectedValue.ToString();
            sFCCompanyCode = CBOFCCompany.SelectedValue.ToString();
            iNumberOfJNBProjects = 0;
            iNumberOfMMProjects = 0;
            iNumberOfJBAProjects = 0;

            if (sProjCompanyCode == "JNB")
            {
                iNumberOfJNBProjects = Convert.ToInt32(TXTNoJNBProjects.Text);
            }

            if (sFCCompanyCode == "JBAB")
            {
                iNumberOfJBAProjects = Convert.ToInt32(TXTNoOfJBAProjects.Text);
            }

            if (sFCCompanyCode == "MMB")
            {
                iNumberOfMMProjects = Convert.ToInt32(TXTNoMMProjects.Text);
            }

            if (iNumberOfJBAJNBProjects > iNumberOfJNBProjects)
            {
                MessageBox.Show("Number of JBA projects cannot exceed the number of JNB projects.");
                return;
            }

            if (iNumberOfMMJNBProjects > iNumberOfJNBProjects)
            {
                MessageBox.Show("Number of MM projects cannot exceed the number of JNB projects.");
                return;
            }

            if (!RBCopyFrameWorkContract.Checked)
            {
                sSourceFC = "";
            }
            else
            {
                sSourceFC = CBOFrameworkContract.SelectedValue.ToString();
            }

            if (RBProposal.Checked || RBProject.Checked)
            {
                sSourceProjectCode = CBOProjectProposal.SelectedValue.ToString();
            }
            else
            {
                sSourceProjectCode = "";
            }

            sTargetProjectCode = TXTNewProjectCode.Text;
            sProjCompanyCode = CBOProjCompany.SelectedValue.ToString();
            sTargetFC = TXTNewFrameWorkContract.Text;
            sFCCompanyCode = CBOFCCompany.SelectedValue.ToString();
            sFCTitle = TXTFCTitle.Text;

            if (!RBProposal.Checked && !RBProject.Checked)
            {
                MessageBox.Show("Please select either to create a proposal or a project.");
                return;
            }

            if (RBProposal.Checked)
            {
                sProjectType = "O";
            }

            if (RBProject.Checked)
            {
                sProjectType = "P";
            }

            //Check the Framework Contract Code is in the correct format.

            if (RBCopyFrameWorkContract.Checked)
            {
                using (SqlConnection conn = new SqlConnection())
                {
                    conn.ConnectionString = sConnectionString;
                    conn.Open();

                    SqlCommand cmd = new SqlCommand("Utility.ValidateCodeStructure", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter InputParameter = new SqlParameter("@sInputValue", SqlDbType.Char);
                    InputParameter.Value = TXTNewFrameWorkContract.Text;
                    InputParameter.Direction = ParameterDirection.Input;
                    cmd.Parameters.Add(InputParameter);

                    SqlParameter ResultParameter = new SqlParameter("@return_value", SqlDbType.Int);
                    ResultParameter.Direction = ParameterDirection.ReturnValue;
                    cmd.Parameters.Add(ResultParameter);

                    cmd.ExecuteNonQuery();

                    iResult = (int)cmd.Parameters["@return_value"].Value;
                }

                if (iResult > 0)
                {
                    MessageBox.Show("Please enter the Framework Contract code in the correct format.");
                    return;
                }

                using (SqlConnection conn = new SqlConnection())
                {
                    conn.ConnectionString = sConnectionString;
                    conn.Open();

                    SqlCommand cmd = new SqlCommand("Utility.CheckFCExists", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter InputParameter = new SqlParameter("@sInputValue", SqlDbType.Char);
                    InputParameter.Value = TXTNewFrameWorkContract.Text;
                    InputParameter.Direction = ParameterDirection.Input;
                    cmd.Parameters.Add(InputParameter);

                    SqlParameter FCCompParameter = new SqlParameter("@sFCCompanyCode", SqlDbType.Char);
                    FCCompParameter.Value = CBOFCCompany.SelectedValue.ToString();
                    FCCompParameter.Direction = ParameterDirection.Input;
                    cmd.Parameters.Add(FCCompParameter);

                    SqlParameter ResultParameter = new SqlParameter("@return_value", SqlDbType.Int);
                    ResultParameter.Direction = ParameterDirection.ReturnValue;
                    cmd.Parameters.Add(ResultParameter);

                    cmd.ExecuteNonQuery();

                    iResult = (int)cmd.Parameters["@return_value"].Value;
                }

                if (iResult > 0)
                {
                    MessageBox.Show("The entered Framework Contract code already exists - Please correct.");
                    return;
                }
            }
            //Check the Project Code is in the correct format.

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = sConnectionString;
                conn.Open();

                SqlCommand cmd = new SqlCommand("Utility.ValidateCodeStructure", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter InputParameter = new SqlParameter("@sInputValue", SqlDbType.Char);
                InputParameter.Value = TXTNewProjectCode.Text;
                InputParameter.Direction = ParameterDirection.Input;
                cmd.Parameters.Add(InputParameter);

                SqlParameter ResultParameter = new SqlParameter("@return_value", SqlDbType.Int);
                ResultParameter.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(ResultParameter);

                cmd.ExecuteNonQuery();

                iResult = (int)cmd.Parameters["@return_value"].Value;
            }

            if (iResult > 0)
            {
                MessageBox.Show("Please enter the Project code in the correct format.");
                return;
            }

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = sConnectionString;
                conn.Open();

                SqlCommand cmd = new SqlCommand("Utility.FindNextProjectCode", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter PatternParameter = new SqlParameter("@sPatternValue", SqlDbType.Char);
                PatternParameter.Value = sTargetProjectCode;
                cmd.Parameters.Add(PatternParameter);

                SqlParameter HowManyParameter = new SqlParameter("@iHowMany", SqlDbType.Int);
                HowManyParameter.Value = iNumberOfJNBProjects;
                cmd.Parameters.Add(HowManyParameter);

                SqlParameter StartParameter = new SqlParameter("@sReturnValue", SqlDbType.VarChar);
                StartParameter.Direction = ParameterDirection.Output;
                StartParameter.Size = 7;
                cmd.Parameters.Add(StartParameter);

                SqlParameter RetValParameter = new SqlParameter("@return_value", SqlDbType.Int);
                RetValParameter.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(RetValParameter);

                cmd.ExecuteNonQuery();

                cmd.Dispose();
                conn.Close();
            }

            FrmDefineRoles RolesForm = new FrmDefineRoles(iEnteredNoOfProjectProposals,
                sSourceFC,
                sTargetFC,
                sTargetProjectCode,
                sSourceProjectCode,
                sProjCompanyCode,
                sFCCompanyCode,
                sProjectType,
                iNumberOfJNBProjects,
                iNumberOfMMProjects,
                iNumberOfJBAProjects,
                sFCTitle);
            RolesForm.Show();
        }

        private void BTNExit_Click(object sender, EventArgs e)
        {
            string message = "Are you sure you wish to exit this application?";
            string caption = "Exit Application";
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult result;

            result = MessageBox.Show(message, caption, buttons);

            if (result == DialogResult.Yes)
            {
                sConnectionString = Properties.Settings.Default.ExternalDataConnectionString;
                sOriginator = Environment.UserDomainName + "\\" + Environment.UserName;

                using (SqlConnection conn = new SqlConnection())
                {
                    conn.ConnectionString = sConnectionString;
                    conn.Open();

                    SqlCommand cmd = new SqlCommand("Utility.PrjCpyCleanup", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter OriginatorParameter = new SqlParameter("@Originator", SqlDbType.Char);
                    OriginatorParameter.Value = sOriginator;
                    cmd.Parameters.Add(OriginatorParameter);

                    cmd.ExecuteNonQuery();
                    conn.Close();

                    Application.Exit();
                }
            }
        }

        private void TXTNoJNBProjects_TextChanged(object sender, EventArgs e)
        {
            if (CBOProjCompany.SelectedValue.ToString() == "JNB")
            {
                if (TXTNoJNBProjects.Text.Length == 0)
                {
                    MessageBox.Show("Please enter the number of JNB project(s)/Proposal(s) to create");
                    return;
                }

                if (int.TryParse(TXTNoJNBProjects.Text, out iNumberOfJNBProjects))
                {
                    iNumberOfJNBProjects = Convert.ToInt32(TXTNoJNBProjects.Text);
                }
                else
                {
                    MessageBox.Show("Please enter a number");
                }
            }
        }

        private void TXTNoMMProjects_TextChanged(object sender, EventArgs e)
        {
            if (TXTNoMMProjects.Text.Length == 0)
            {
                MessageBox.Show("Please enter the number of MM project(s) to create.");
                TXTNoOfJBAProjects.Enabled = true;
                TXTNoOfJBAProjects.Text = TXTNoJNBProjects.Text;
                return;
            }

            if (int.TryParse(TXTNoMMProjects.Text, out iNumberOfProjectsProposals))
            {
                iNumberOfMMProjects = Convert.ToInt32(TXTNoMMProjects.Text);
            }
            else
            {
                MessageBox.Show("Please enter a number.");
            }

            if (TXTNoMMProjects.Text.Length > 0)
            {
                TXTNoOfJBAProjects.Text = "";
                TXTNoOfJBAProjects.Enabled = false;
            }
        }
        private void TXTNoOfJBAProjects_TextChanged(object sender, EventArgs e)
        {
            if (TXTNoOfJBAProjects.Text.Length == 0)
            {
                MessageBox.Show("Please enter the number of JBA project(s) to create.");
                TXTNoMMProjects.Enabled = true;
                TXTNoMMProjects.Text = TXTNoJNBProjects.Text;
                return;
            }

            if (int.TryParse(TXTNoOfJBAProjects.Text, out iNumberOfProjectsProposals))
            {
                iNumberOfJBAProjects = Convert.ToInt32(TXTNoOfJBAProjects.Text);
            }
            else
            {
                MessageBox.Show("Please enter a number.");
            }

            if (TXTNoOfJBAProjects.Text.Length > 0)
            {
                TXTNoMMProjects.Enabled = false;
                TXTNoMMProjects.Text = "";
            }
        }

        private void Form1_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            string message = "Are you sure you wish to exit this application?";
            string caption = "Exit Application";
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult result;

            result = MessageBox.Show(message, caption, buttons);

            if (result == DialogResult.Yes)
            {
                sConnectionString = Properties.Settings.Default.ExternalDataConnectionString;
                sOriginator = Environment.UserDomainName + "\\" + Environment.UserName;

                using (SqlConnection conn = new SqlConnection())
                {
                    conn.ConnectionString = sConnectionString;
                    conn.Open();

                    SqlCommand cmd = new SqlCommand("Utility.PrjCpyCleanup", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter OriginatorParameter = new SqlParameter("@Originator", SqlDbType.Char);
                    OriginatorParameter.Value = sOriginator;
                    cmd.Parameters.Add(OriginatorParameter);

                    cmd.ExecuteNonQuery();
                    conn.Close();

                    Application.Exit();
                }
            }
        }
        private void TXTNewProjectCode_TextChanged(object sender, EventArgs e)
        {

        }

        private void TXTFCTitle_TextChanged(object sender, EventArgs e)
        {

        }
        private void TXTNewFrameWorkContract_LostFocus(object sender, EventArgs e)
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = sConnectionString;
                conn.Open();

                SqlCommand cmd = new SqlCommand("Utility.FindNextFrameworkContract", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter PatternParameter = new SqlParameter("@sPatternValue", SqlDbType.Char);
                PatternParameter.Value = TXTNewFrameWorkContract.Text;
                cmd.Parameters.Add(PatternParameter);

                SqlParameter ReturnValue = new SqlParameter("@sReturnValue", SqlDbType.Char);
                ReturnValue.Direction = ParameterDirection.Output;
                ReturnValue.Size = 7;
                cmd.Parameters.Add(ReturnValue);

                cmd.ExecuteNonQuery();
                conn.Close();

                TXTNewFrameWorkContract.Text = cmd.Parameters["@sReturnValue"].Value.ToString().Replace(" ", "");
                TXTNewProjectCode.Text = TXTNewFrameWorkContract.Text;
            }
        }
        private void TXTNewFrameWorkContract_TextChanged(object sender, EventArgs e)
        {
            TXTNewFrameWorkContract.Text = TXTNewFrameWorkContract.Text.ToUpper();
        }
    }
}
